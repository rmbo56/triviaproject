﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

static class Define
{
    public const string serverIP = "127.0.0.1";
    public const int serverPort = 5566;
}

public struct User
{
    public string user_name;
    public string password;
}

namespace GUIproject
{
    public class Communicator
    {
        private User _user;
        private TcpClient _client;
        private NetworkStream _clientStream;

        public Communicator()
        {
            _client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 5566);
            _client.Connect(serverEndPoint);
        }

        public void setUser(string user_name, string password)
        {
            _user.user_name = user_name;
            _user.password = password;
        }

        public User getUser()
        {
            return _user;
        }

        public bool send_msg_to_server(string msg, int code_to_server)
        {
            try//if can sent a msg to the server will return true
            {
                // Encode the data string into a byte array.  
                string msg_to_server = "";
                msg_to_server += (char)code_to_server;

                //will make the data length 4 bytes 
                byte[] bytes = BitConverter.GetBytes(msg.Length);
                if (BitConverter.IsLittleEndian)
                {
                    Array.Reverse(bytes);
                }
                //convert the byte to string
                msg_to_server += System.Text.Encoding.Default.GetString(bytes);
                msg_to_server += msg;

                _clientStream = _client.GetStream();
                byte[] buffer = new ASCIIEncoding().GetBytes(msg_to_server);
                _clientStream.Write(buffer, 0, buffer.Length);
                _clientStream.Flush();
                return true;
            }
            catch (Exception e)//if can't sent a msg to the server will return false
            {
                return false;
            }
        }

        public string get_msg_from_server()
        {
            try
            {
                // Data buffer for incoming data.  
                byte[] buffer = new byte[4096];
                int bytesRead = _clientStream.Read(buffer, 0, 4096);
                string undeciphered_msg = System.Text.Encoding.ASCII.GetString(buffer, 0, bytesRead);
                return undeciphered_msg.Remove(0, 5);//will return the msg from the server without the msg size and type code
            }
            catch (Exception e)
            {
                return "-1";
            }
        }
    }
}
