﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace GUIproject
{
    /// <summary>
    /// Interaction logic for CreateRoomWindow.xaml
    /// </summary>
    public partial class CreateRoomWindow : Window
    {
        private Communicator _client;
        public CreateRoomWindow(Communicator client)
        {
            this._client = client;
            InitializeComponent();
        }

        private void btnCreateRoom_Click(object sender, RoutedEventArgs e)
        {
            if (!AllDataCheck())
            {
                roomNameBox.Text = "";
                timeBox.Text = "";
                amountPlayersBox.Text = "";
                amountQuestionsBox.Text = "";
            }
            else
            {
                CreateRoom room_data = new CreateRoom();
                room_data.answerTimeout = int.Parse(timeBox.Text);
                room_data.maxUsers = int.Parse(amountPlayersBox.Text);
                room_data.questionCount = int.Parse(amountQuestionsBox.Text);
                room_data.roomName = roomNameBox.Text;

                string json = JsonConvert.SerializeObject(room_data);

                _client.send_msg_to_server(json, 55);

                string msg_from_server = _client.get_msg_from_server();

                if (msg_from_server.Contains("status"))//good
                {
                    //will move to the room after creating it!
                    RoomJoiningWindow rgw = new RoomJoiningWindow(_client, roomNameBox.Text);
                    this.Close();
                    rgw.ShowDialog();
                }
                else
                {
                    if (msg_from_server == "-1")
                    {
                        MessageBox.Show("error", "oops", MessageBoxButton.OK, MessageBoxImage.Error);
                        roomNameBox.Text = "";
                        timeBox.Text = "";
                        amountPlayersBox.Text = "";
                        amountQuestionsBox.Text = "";
                    }
                    else
                    {
                        Error error_msg = JsonConvert.DeserializeObject<Error>(msg_from_server);

                        MessageBox.Show(error_msg.message, "oops", MessageBoxButton.OK, MessageBoxImage.Error);
                        roomNameBox.Text = "";
                        timeBox.Text = "";
                        amountPlayersBox.Text = "";
                        amountQuestionsBox.Text = "";
                    }
                }

                
            }
        }



        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            //will move to the menu back
            menuWindow next = new menuWindow(this._client);
            this.Close();
            next.ShowDialog();
        }



        private bool AllDataCheck()
        {
            
            if (roomNameBox.Text == "" || timeBox.Text == "" || amountPlayersBox.Text == "")
            {
                MessageBox.Show("You need to write on all the fields! ", "are you stupid?", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            //checking if the time and the anount of players fields are numbers
            else if (isNumber(timeBox.Text) == false || isNumber(amountPlayersBox.Text) == false || isNumber(amountQuestionsBox.Text) == false)
            {
                MessageBox.Show("this field is only for number! ", "are you stupid?", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            //**this part will check if there is name like one of the name in the all list **
            _client.send_msg_to_server("void", 56); //requesting from the server all the rooms

            string msg_from_server = _client.get_msg_from_server(); //getting all the rooms from the server
            string[] save_all = msg_from_server.Split(',');

            string save_room_name_as_string = "\"name\":" +'"'+ roomNameBox.Text + '"'; //will save the room name with: "name"
            

            for (int t = 0; t < save_all.Length; t++)
            {
                //string save_curr = save_all[t].Substring(7, (save_all[t].Length) - 7);
                
                if (string.Equals(save_all[t], save_room_name_as_string))
                {
                    MessageBox.Show("There is A room with this name allready! ", "are you stupid?", MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }
            }

            return true;
        }

        //a helper function that checking if the field is a number
        private bool isNumber(String text)
        {
            for (int i = 0; i < text.Length; i++)
                if (char.IsDigit(text[i]) == false)
                {
                    return false;
                }

            return true;
        }

    }
}
