﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace GUIproject
{
    /// <summary>
    /// Interaction logic for HighScoresWindow.xaml
    /// </summary>
    public partial class HighScoresWindow : Window
    {
        private Communicator _client;
        public HighScoresWindow(Communicator client)
        {
            this._client = client;
            InitializeComponent();
            MadeHighScoreBoard();
        }

        private void MadeHighScoreBoard()
        {
            _client.send_msg_to_server("void", 58);
            string msg = _client.get_msg_from_server();
            if(msg.Contains("message"))
            {
                MessageBox.Show(msg, "oops", MessageBoxButton.OK, MessageBoxImage.Error);//leave error message
            }
            else
            {
                msg = msg.Remove(0, 16);// removing the "{\"statistics\":[\ from the msg
                for (int i = 0; i < 2; i++)//remove the \"]}" from the msg
                {
                    msg = msg.Remove(msg.Length - 1, 1);
                }
                
                string[] list = msg.Split(',');

                //remove the "" from the data
                list[0] = list[0].Remove(list[0].Length - 1, 1);
                for (int i = 1; i < list.Length; i++)
                {
                    list[i] = list[i].Remove(0, 1);
                    list[i] = list[i].Remove(list[i].Length - 1, 1);

                }
                //will put the scores in the board
                for (int i = 0; i < list.Length; i++)
                {
                    string[] stringArray = list[i].Split(':');
                    if (i == 0) { p1.Text = stringArray[1];}
                    if (i == 1) { p2.Text = stringArray[1];}
                    if (i == 2) { p3.Text = stringArray[1];}
                    if (i == 3) { p4.Text = stringArray[1];}
                    if (i == 4) { p5.Text = stringArray[1];}
                }
            }
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            //will move to the menu back
            menuWindow next = new menuWindow(this._client);
            this.Close();
            next.ShowDialog();
        }
    }
}
