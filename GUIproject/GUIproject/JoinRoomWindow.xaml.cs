﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;


namespace GUIproject
{
    /// <summary>
    /// Interaction logic for JoinRoomWindow.xaml
    /// </summary>
    public partial class JoinRoomWindow : Window
    {
        private Communicator _client;
        public JoinRoomWindow(Communicator client)
        {
            this._client = client;
            InitializeComponent();

            getAllRooms();

        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            //will move to the menu back
            menuWindow next = new menuWindow(this._client);
            this.Close();
            next.ShowDialog();
        }

        //a function that getting the info about the specific room and showing them to the screen 
        private void JoinRoomButton_Click(object sender, RoutedEventArgs e)
        {
            string roomName = OpenRooms.SelectedItem.ToString();


            bool save=addUserToRoom(roomName);
            if(save)
            {
                RoomJoiningWindow rgw = new RoomJoiningWindow(_client, roomName);
                this.Close();
                rgw.ShowDialog();
            }
            
        }

        //a function that adding the user to the room ! (joining the : joinRoom option in the cpp code)
        private bool addUserToRoom(string roomName)
        {
            RoomName request = new RoomName();// create new request
            request.roomName = roomName;
            string json = JsonConvert.SerializeObject(request);// serialize request objet into json
            _client.send_msg_to_server(json, 54);

            string serverResponse = _client.get_msg_from_server();// gets server response

            if (serverResponse.Contains("message"))//error
            {
                Error error_msg = JsonConvert.DeserializeObject<Error>(serverResponse);

                MessageBox.Show(error_msg.message,"oops", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return true;
        }

        //a function that getting all the rooms from the server , spliting them and adding their names to the listBox
        private void getAllRooms()
        {
            //need to get all the rooms from the server (DATA BASE)
            //than we need to go over all the rooms and add them to the list that called : "OpenRooms"

            _client.send_msg_to_server("void",56); //requesting from the server all the rooms

            string msg_from_server = _client.get_msg_from_server(); //getting all the rooms from the server

            if (msg_from_server.Contains("message"))//error
            {
                MessageBox.Show("error", "oops", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            else
            {
                //a loop that goona all over the string that got from the server and needs to put every place in the list "OpenRooms"

                
                int save_length = (msg_from_server.Length) - 12;
                msg_from_server = msg_from_server.Substring(10, save_length); //remove the first 10 letters and the last 2 letters

            
               string splitter = "},";
               string[] saveRooms = msg_from_server.Split(new string[] { splitter }, StringSplitOptions.None); //split the msg according to the '},'
               
                //a loop that will add to all the places the last '}'
                for(int j=0;j<saveRooms.Length-1;j++)
                {
                    saveRooms[j] += '}';
                }
                //-----------------------------------------------------------------

                //if there are no rooms 
                if(saveRooms[0]=="")
                {
                    MessageBox.Show("there are no open rooms yet!", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    List<string> roomsMy = new List<string>();
                    //a loop that going over all the save rooms array and adding each room to the openRooms .items
                    for (int i = 0; i < saveRooms.Length; i++)
                    {
                        GetRooms save = JsonConvert.DeserializeObject<GetRooms>(saveRooms[i]);
                        roomsMy.Add(save.name.ToString()); //adding the name to the list 
                        OpenRooms.ItemsSource = roomsMy;
                    }
                }
            }

            //a loop that going over the open rooms and checking if the user choose a room and click in the "join" button so we need to move him to the room that shows the info about the room
            ListViewItem temp;
            foreach (var item in OpenRooms.Items)
            {
                temp = new ListViewItem();
                temp.Content = item;
                temp.AddHandler(ListViewItem.MouseDoubleClickEvent, new RoutedEventHandler(JoinRoomButton_Click)); //after we click in the join button we will take into a room with all the info about the specific room
            }
                
        }


    }
}
