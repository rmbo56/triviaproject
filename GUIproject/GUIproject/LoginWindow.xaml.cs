﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Newtonsoft.Json;

namespace GUIproject
{

    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private Communicator _client;
        public LoginWindow(Communicator client)
        {
            this._client = client;
            InitializeComponent();
        }

        private void btnSignUp_Click(object sender, RoutedEventArgs e)
        {
            SignupWindow next = new SignupWindow(this._client);
            this.Close();
            next.ShowDialog();

        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (!AllDataCheck())
            {
                userNameBox.Text = "";
                passwordBox.Text = "";
            }
            else
            {
                Login user_data = new Login();
                user_data.username = userNameBox.Text;
                user_data.password = passwordBox.Text;

                string json = JsonConvert.SerializeObject(user_data);

                _client.send_msg_to_server(json, 51);

                string msg_from_server = _client.get_msg_from_server();

                if (msg_from_server.Contains("status"))//good
                {
                    //will go to the menu
                    this._client.setUser(user_data.username, user_data.password);//adding the user to the communicator

                    menuWindow next = new menuWindow(this._client);
                    this.Close();
                    next.ShowDialog();
                }
                else
                {
                    if(msg_from_server == "-1")
                    {
                        MessageBox.Show("error", "oops", MessageBoxButton.OK, MessageBoxImage.Error);
                        userNameBox.Text = "";
                        passwordBox.Text = "";
                    }
                    else
                    {
                        Error error_msg = JsonConvert.DeserializeObject<Error>(msg_from_server);

                        MessageBox.Show(error_msg.message, "oops", MessageBoxButton.OK, MessageBoxImage.Error);
                        userNameBox.Text = "";
                        passwordBox.Text = "";
                    }   
                }

            }
        }


        private bool AllDataCheck()
        {
            if (userNameBox.Text == "" || passwordBox.Text == "")
            {
                MessageBox.Show("You need to put information about the user", "are you stupid?", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }
    }

}
