﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUIproject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Communicator _client;
        public MainWindow()
        {
            _client = new Communicator();
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            LoginWindow next = new LoginWindow(this._client);
            //next.RaiseCustomEvent += new EventHandler<CustomEventArgs>(newWindow_RaiseCustomEvent);
            this.Close();
            next.ShowDialog();
        }

        private void btnSignup_Click(object sender, RoutedEventArgs e)
        {
            SignupWindow next = new SignupWindow(this._client);
            this.Close();
            next.ShowDialog();
        }
    }
}
