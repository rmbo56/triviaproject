﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


using Newtonsoft.Json;

namespace GUIproject
{
    /// <summary>
    /// Interaction logic for RoomJoiningWindow.xaml
    /// </summary>
    public partial class RoomJoiningWindow : Window
    {
        private Communicator _client;
        public RoomJoiningWindow(Communicator client, string roomName)
        {
            this._client = client;
            InitializeComponent();

            RoomInfo(roomName);
        }

       

        private void RoomInfo(string roomName)
        {
            RoomInfoRequest infoRequest = new RoomInfoRequest();// create new request
            infoRequest.roomName = roomName;

            string json = JsonConvert.SerializeObject(infoRequest);// serialize request objet into json

            //Send request for room's information
            _client.send_msg_to_server(json, 60);

            string serverResponse = _client.get_msg_from_server();// gets server response

            if (serverResponse.Contains("message"))//error
            {
                MessageBox.Show("error", "oops", MessageBoxButton.OK, MessageBoxImage.Error);
                JoinRoomWindow jrw = new JoinRoomWindow(this._client);
                this.Close();
                jrw.ShowDialog();
            }
            else
            {
                GetRooms roomInfo = JsonConvert.DeserializeObject<GetRooms>(serverResponse);
                Name.Text += roomInfo.name + " #" + roomInfo.id;
                Admin.Text += roomInfo.admin;
                MaxPlayers.Text += roomInfo.maxPlayers;
                NumberOfQuestions.Text += roomInfo.numOfQuestionsInGame;
                TimePerQuestion.Text += roomInfo.timePerQuestion;
                if(roomInfo.isActive == 0)
                {
                    Status.Text += "Not Active";
                }
                else
                {
                    Status.Text += "Active";
                }

                //a list that shows all the members in the room
                getListOfPlayers(roomName);
            }

        }
        //a function that send a request to the server to get all the players in the room and adding him to the list that called "AllPlayers"
        private void getListOfPlayers(string roomName)
        {
            GetPlayersInRoom request = new GetPlayersInRoom();// create new request
            request.roomName = roomName;

            string save_json = JsonConvert.SerializeObject(request);// serialize request objet into json

            //Send request for all the players in the room
            _client.send_msg_to_server(save_json, 57);

            string serverResponseAllPlayers = _client.get_msg_from_server();// gets server response

            int save_length = (serverResponseAllPlayers.Length) - 12;
            serverResponseAllPlayers = serverResponseAllPlayers.Substring(11, save_length); //remove the first 11 letters and the last 1 letter

            string[] savePlayersInRoom = serverResponseAllPlayers.Split(",", StringSplitOptions.None);

            List<string> myPlayers = new List<string>();
            string save_name_good = "";
            //a loop that going over all the save rooms array and adding each room to the openRooms .items
            for (int i = 0; i < savePlayersInRoom.Length; i++)
            {
                string save_curr_name = savePlayersInRoom[i];
                

                //a loop that going over the letters and check if the letters are good
                for (int j = 0; j < save_curr_name.Length; j++)
                {
                    if(save_curr_name[j]!='[' && save_curr_name[j] !=']' && save_curr_name[j] !='"')
                    {
                        save_name_good += save_curr_name[j];
                    }
                }

                myPlayers.Add(save_name_good.ToString()); //adding the name to the list 
                AllPlayers.ItemsSource = myPlayers;
                save_name_good = ""; //reset the name
            }
        }


        private void returnButton_Click(object sender, RoutedEventArgs e)
        {
            JoinRoomWindow jrw = new JoinRoomWindow(this._client);
            this.Close();
            jrw.ShowDialog();
        }

        private void StartButtonClick(object sender, RoutedEventArgs e)
        {
            if(Admin.Text.Contains(_client.getUser().user_name))// if admin user press to start
            {
                ///Go into game window
            }
        }
    }
}
