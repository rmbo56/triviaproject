﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace GUIproject
{
    /// <summary>
    /// Interaction logic for SelfStatisticsWindow.xaml
    /// </summary>
    public partial class SelfStatisticsWindow : Window
    {
        private Communicator _client;
        public SelfStatisticsWindow(Communicator client)
        {
            this._client = client;
            InitializeComponent();

            MadeStatistics();
        }

        private void MadeStatistics()
        {
            _client.send_msg_to_server("void", 59);
            string msg_from_server = _client.get_msg_from_server();
           

            if (msg_from_server.Contains("message"))//error
            {
                MessageBox.Show("Failed to find user statistics", "oops", MessageBoxButton.OK, MessageBoxImage.Error);
                amountOfGamesData.Text = "-";
                totalAnswersData.Text = "-";
                totalCorrectAnswerData.Text = "-";
                averageAnswerTimeData.Text = "-";
            }
            else
            {
                SelfStatistics stat = JsonConvert.DeserializeObject<SelfStatistics>(msg_from_server);
                amountOfGamesData.Text = stat.games.ToString();
                totalAnswersData.Text = stat.total.ToString();
                totalCorrectAnswerData.Text = stat.correct.ToString();
                averageAnswerTimeData.Text = stat.avgTime.ToString();
            }

        }



        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            //will move to the menu back
            menuWindow next = new menuWindow(this._client);
            this.Close();
            next.ShowDialog();
        }
    }
}
