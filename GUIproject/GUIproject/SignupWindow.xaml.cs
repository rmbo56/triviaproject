﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


using Newtonsoft.Json;

namespace GUIproject
{
    /// <summary>
    /// Interaction logic for SignupWindow.xaml
    /// </summary>
    public partial class SignupWindow : Window
    {
        private Communicator _client;
        public SignupWindow(Communicator client)
        {
            this._client = client;
            InitializeComponent();
        }

        private void btnSignup_Click(object sender, RoutedEventArgs e)
        {
            if(!AllDataCheck())
            {
                emaildBox.Text = "";
                userNameBox.Text = "";
                passwordBox.Text = "";
            }
            else
            {
                //assign data fields into Signup struct
                Signup userData = new Signup();
                userData.username = userNameBox.Text;
                userData.password = passwordBox.Text;
                userData.email = emaildBox.Text;

                string intoJson = JsonConvert.SerializeObject(userData); //convet struct into json string

                _client.send_msg_to_server(intoJson, 52);// send message to server
                string msg_from_server = _client.get_msg_from_server();// recieve response message from server

                if (msg_from_server.Contains("status"))// in case of good response - exit into main window
                {
                    LoginWindow next = new LoginWindow(this._client);
                    this.Close();
                    MessageBox.Show("Account Created Successfully!", "Successfully", MessageBoxButton.OK, MessageBoxImage.Information);
                    next.ShowDialog();
                }
                else // in case of error - display error to user
                {
                    if(msg_from_server == "-1")
                    {
                        MessageBox.Show("error", "oops", MessageBoxButton.OK, MessageBoxImage.Error);
                        userNameBox.Text = "";
                        passwordBox.Text = "";
                    }
                    else
                    {
                        Error error_msg = JsonConvert.DeserializeObject<Error>(msg_from_server);

                        MessageBox.Show(error_msg.message, "oops", MessageBoxButton.OK, MessageBoxImage.Error);
                        userNameBox.Text = "";
                        passwordBox.Text = "";
                        emaildBox.Text = "";
                    }                    
                }
            }

        }

        private void btnReturnToStart_Click(object sender, RoutedEventArgs e)
        {
            MainWindow next = new MainWindow();
            this.Close();
            next.ShowDialog();
        }

        private bool AllDataCheck()
        {
            if (userNameBox.Text == "" || passwordBox.Text == "" || emaildBox.Text == "")
            {
                MessageBox.Show("You need to put information about the user", "are you stupid?", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }
    }
}   
