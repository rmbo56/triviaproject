﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIproject
{
    /// <summary>
    /// Interaction logic for StatisticsWindow.xaml
    /// </summary>
    public partial class StatisticsWindow : Window
    {
        private Communicator _client;
        public StatisticsWindow(Communicator client)
        {
            this._client = client;
            InitializeComponent();
        }

        private void btnTopTable_Click(object sender, RoutedEventArgs e)
        {
            //will move to the top table window
            HighScoresWindow next = new HighScoresWindow(this._client);
            this.Close();
            next.ShowDialog();

        }
        private void btnSelf_Click(object sender, RoutedEventArgs e)
        {
            //will move to the self statistics window
            SelfStatisticsWindow next = new SelfStatisticsWindow(this._client);
            this.Close();
            next.ShowDialog();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            //will move to the menu back
            menuWindow next = new menuWindow(this._client);
            this.Close();
            next.ShowDialog();
        }
    }

    
}
