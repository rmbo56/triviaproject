﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUIproject
{
    /// <summary>
    /// Interaction logic for menuWindow.xaml
    /// </summary>
    public partial class menuWindow : Window
    {
        private Communicator _client;
        public menuWindow(Communicator client)
        {
            this._client = client;
            InitializeComponent();
        }

        private void btnCreateRoom_Click(object sender, RoutedEventArgs e)
        {
            //will move to the create new room window
            CreateRoomWindow next = new CreateRoomWindow(this._client);
            this.Close();
            next.ShowDialog();
        }

        private void btnJoinRoom_Click(object sender, RoutedEventArgs e)
        {
            //will move to the join new room window
            JoinRoomWindow next = new JoinRoomWindow(this._client);
            this.Close();
            next.ShowDialog();
        }

        private void btnShowStatistics_Click(object sender, RoutedEventArgs e)
        {
            //will move to the statistics room
            StatisticsWindow next = new StatisticsWindow(this._client);
            this.Close();
            next.ShowDialog();
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            //will exit the game
            this.Close();
        }

    }
}
