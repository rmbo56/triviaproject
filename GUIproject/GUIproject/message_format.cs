﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUIproject
{
    struct Signup
    {
        public string username;
        public string password;
        public string email;
    }
    struct Login
    {
        public string username;
        public string password;
    }
    struct Error
    {
        public string message;
    }
    struct CreateRoom
    {
        public int answerTimeout;
        public int maxUsers;
        public int questionCount;
        public string roomName;
    }
    struct JoinRoom
    {
        public string userName;
    }

    struct GetRooms
    {
        public string admin;
        public int id;
        public string name;
        public int maxPlayers;
        public int numOfQuestionsInGame;
        public int timePerQuestion;
        public int isActive;
    }

    struct GetPlayersInRoom
    {
        public string roomName;
    }

    struct RoomInfoRequest
    {
        public string roomName;
    }

    struct RoomName
    {
        public string roomName;
    }


    struct SelfStatistics
    {
        public string avgTime;//float
        public string correct;
        public string games;
        public string total;
    }

    struct TopFive
    {
        public string place1;
        public string place2;
        public string place3;
        public string place4;
        public string place5;
    }
}
