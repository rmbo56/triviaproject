import socket
import json

SERVER_IP = "127.0.0.1" #Local Computer IP
SERVER_PORT = 1234 #We can change it later.....
MESSAGE = "Hello" #To test sending operation
LOGIN_CODE = 51
SIGNUP_CODE = 52
LENGTH_FILED_SIZE = 4

def generate_login_request(username, password, is_login, mail = ""):

    login_dict = {"username" : username, "password" : password}

    if(is_login):
        message = chr(LOGIN_CODE)
    else:
        message = chr(SIGNUP_CODE)
        login_dict["email"] = mail

    json_part = json.dumps(login_dict)
    print(json_part)
    #add '0' if length of field is less than it's format
    if len(chr(len(str(json_part)))) < LENGTH_FILED_SIZE:
        for i in range(LENGTH_FILED_SIZE - len(chr(len(str(json_part))))):
            message += '0'

    message += chr(len(str(json_part)))
    message += json_part
    return message

def login_request():
    option = input("To signup enter 's'\nTo login enter 'l'")

    if(option.lower() != 'l' and option.lower() != 's'):
        print("Wrong input")
        return

    elif(option == 'l'):
        username = input("Enter username: ")
        password = input("Enter password: ")
        return generate_login_request(username, password, True)
    else:
        username = input("Enter username: ")
        password = input("Enter password: ")
        mail = input("Enter e-mail: ")
        return generate_login_request(username, password, False, mail)
    


def create_socket(SERVER_IP, SERVER_PORT):
    """
    This fucntion creates client socket to comunicate with the server
    :param SERVER_IP: (str) - ip of server
    :param SERVER_PORT: (int) - listening port of the server
    :return: (socket object) - socket
    """
    #create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connecting to remote computer 7777
    server_address = (SERVER_IP, SERVER_PORT)
    sock.connect(server_address)

    return sock

def get_server_message(sock):
    """
    This function gets socket and return message from its server
    :param sock : (socket) - socket of server with client
    :return : (str) - server's message
    """
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()

    return server_msg

def make_client_message():
    """
    This function responsible for making the message the client will send
    :return : (str) - message with valid syntax, ready to be send 
    """
    msg = MESSAGE

    return msg

def send_message(socket, message):
    """
    This function sends message to server.
    :param: socket (socket) - connection socket with server.
    :param: message (str) - message to send to server
    :return : NONE
    """
    socket.sendall(message.encode())



def main():
    try:
        #create socket by fucntion
        sock = create_socket(SERVER_IP, SERVER_PORT)
    except Exception as e:
        print("Client couldn't make a connection with socket..")
        message_to_send = login_request()
        print("Message sent to server: " + message_to_send)
        return

    while(True):
        #send message to server
        message_to_send = login_request()
        print("Message sent to server: " + message_to_send)
        send_message((sock), message_to_send)

        #get message from server
        msg = get_server_message(sock)
        print(msg)


if __name__ == "__main__":
    main()
