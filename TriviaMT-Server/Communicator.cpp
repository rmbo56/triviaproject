#include "Communicator.h"

Communicator::Communicator(RequestHandlerFactory& handlerFactory):
	m_handlerFactory(handlerFactory)
{
	this->bindAndListen();
}

//will be the server... will open a server and will wait for a client
void Communicator::startHandleRequests()
{
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		SOCKET client_socket = ::accept(this->m_serverSocket, NULL, NULL);
		if (client_socket == INVALID_SOCKET)
		{
			throw std::exception(__FUNCTION__);
		}

		//Client accepted
		//the first stat for the user is the login
		this->m_clients[client_socket] = this->m_clients[client_socket] = new LoginRequestHandler(m_handlerFactory.getLoginManager(), m_handlerFactory);;//insert

		//Creates a thread that handles the conversation with the client
		std::thread clientThread(&Communicator::handleNewClient, this, client_socket);
		clientThread.detach();
	}
}

//will create the server 
void Communicator::bindAndListen()
{
	//will create the server socket
	this->m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->m_serverSocket == INVALID_SOCKET)
	{
		throw std::exception(__FUNCTION__ " - socket");
	}

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(SERVER_PORT);// port that server will listen for
	sa.sin_family = AF_INET;// must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;// when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(this->m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}	

	// Start listening for incoming requests of clients
	if (listen(this->m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - listen");
	}	
	std::cout << "Listening on port " << SERVER_PORT << std::endl;
}

//will handle the client
//input: client socket
//output: none
void Communicator::handleNewClient(SOCKET clientSocket)
{
	try
	{
		while (true)
		{
			JsonResponsePacketSerializer serializer;
			JsonRequestPacketDeserializer deserializer;
			Buffer msg_to_client;
			//will put the msg from the client into a array of char
			char* protocol_msg =  Communicator::getPartFromSocket(clientSocket, 0);

			Buffer forDeserializer = charArrToBuffer(protocol_msg);
			RequestInfo new_requestInfo;
			new_requestInfo.receivalTime = std::time(nullptr);
			new_requestInfo.buffer = forDeserializer;

			//RequestResult ans = new_login.handleRequest(new_requestInfo);
			RequestResult ans = this->m_clients[clientSocket]->handleRequest(new_requestInfo);

			//updating the request stage of the user
			this->m_clients[clientSocket] = ans.newHandler;
			//will send the msg to the client
			Communicator::sendData(clientSocket, Communicator::bufferToString(ans.response));
		}
	}
	catch (std::exception& e)//disconnect
	{
		std::cout << "client disconnected" << std::endl;
		////////////////////////////////////
		//making logout request
		unsigned char charMSG_CODE = LogoutRequestID;
		json responseJson;
		responseJson["logout"] = "bye";
		std::string data = responseJson.dump();
		//will make the data length 4 bytes 
		std::string dataLengthSTR = "";
		unsigned char charDATA_LENGTH[4];
		charDATA_LENGTH[3] = (data.length() >> 24) & 0xFF;
		charDATA_LENGTH[2] = (data.length() >> 16) & 0xFF;
		charDATA_LENGTH[1] = (data.length() >> 8) & 0xFF;
		charDATA_LENGTH[0] = data.length() & 0xFF;
		for (int i = 3; i >= 0; i--)
		{
			dataLengthSTR += charDATA_LENGTH[i];
		}
		std::ostringstream oneBytesMSG_CODE;
		oneBytesMSG_CODE << std::setw(1) << std::setfill('0') << charMSG_CODE;

		std::string msg = oneBytesMSG_CODE.str() + dataLengthSTR + data;

		Buffer forDeserializer = Buffer(msg.begin(), msg.end());
		RequestInfo new_requestInfo;
		new_requestInfo.receivalTime = std::time(nullptr);
		new_requestInfo.buffer = forDeserializer;

		this->m_clients[clientSocket]->handleRequest(new_requestInfo);
		/////////////////////////////////////////////////
		//updating the request stage of the user
		this->m_clients[clientSocket] = nullptr;

		closesocket(clientSocket);//close TCP socket with client
	}
}

//will send a msg to a client
//input: client socket, msg for the client
//output: none
void Communicator::sendData(SOCKET sc, const std::string message)
{
	const char* data = message.c_str();

	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

//will get a msg from the client
//input: client socket, flag = 0
//output: msg from the client
char* Communicator::getPartFromSocket(SOCKET sc, int flags)
{
	//will get the code
	char* code = Communicator::recvFromClient(sc, CODE_SIZE_PART);
	//will get the data size
	char* dataSize = Communicator::recvFromClient(sc, DATA_SIZE_PART);
	//will get the data
	char* data = Communicator::recvFromClient(sc, Communicator::getDataSize(dataSize));//char* data = Communicator::recvFromClient(sc, *(int*)dataSize);
	
	char* msg = new char[CODE_SIZE_PART + DATA_SIZE_PART + Communicator::getDataSize(dataSize)];
	//put the code in the msg
	msg[0] = code[0];
	//put the data size
	for (int i = 0; i < DATA_SIZE_PART; i++)
	{
		msg[i + CODE_SIZE_PART] = dataSize[i];
	}
	//put the data
	int save_data_size = Communicator::getDataSize(dataSize);
	for (int i = 0; i < save_data_size; i++)
	{
		unsigned char save = data[i];
		int receiveCode = (int)save;
		msg[i + DATA_SIZE_PART + CODE_SIZE_PART] = data[i];
	}

	return msg;
}

//will make a buffer to a string
//input: buffer
//output: the buffer as a string
std::string Communicator::bufferToString(Buffer buffer)
{
	std::string save_string_part = "";
	//a loop that go over all the buffer from the 5 place (because we have 1 byte (the request code id) , 4 bytes( the length of the data))
	for (std::vector<unsigned char>::iterator it = buffer.begin(); it != buffer.end(); ++it)
	{
		save_string_part += *it; //adding to the all string 
	}
	
	return save_string_part;
}

//will get msg part from the client by the zise
//input: sc->server socket, size->the size of the msg 
//output: char* of the msg
char* Communicator::recvFromClient(SOCKET sc, int size)
{
	char* data = new char[size];
	if (recv(sc, data, size, 0) == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(sc);
		throw std::exception(s.c_str());
	}

	/*
	for (int i = 0; i < size; i++)
	{
		unsigned char save = data[i];
		int receiveCode = (int)save;
		std::cout << (char)receiveCode;// << std::endl;
	}
	*/

	return data;
}

//will give the size data that in an 4 size char*
//input: dataSize-> a 4 size char* 
//output: an int that have a values from the the char* array 
int Communicator::getDataSize(char* dataSize)
{
	int wasVal = 0;
	int size = 0;
	for (int i = 0; i < DATA_SIZE_PART; i++)
	{
		unsigned char save = dataSize[i];
		int receiveCode = (int)save;
		if (wasVal != 0)
		{
			size += receiveCode;
		}
		else if (wasVal == 0 && dataSize[i] != '0')
		{
			wasVal = 1;
			size += receiveCode;
		}

	}
	return size;
}

//make the msg from the client to Buffer from char*
//input: the msg as a char*
//output: the msg as a Buffer
Buffer Communicator::charArrToBuffer(char* data)
{

	char* dataPartSize = new char[DATA_SIZE_PART];
	Buffer dataIn;
	//will put in the buffer the code
	unsigned char save = data[0];
	int receiveCode = (int)save;
	dataIn.push_back((char)receiveCode);

	//will make the part of the data size
	for (int i = 1; i <= DATA_SIZE_PART; i++)
	{
		unsigned char save = data[i];
		int receiveCode = (int)save;
		dataIn.push_back((char)receiveCode);

		dataPartSize[i - CODE_SIZE_PART] = data[i];
	}

	//the data
	for (int i = DATA_SIZE_PART + CODE_SIZE_PART; i < Communicator::getDataSize(dataPartSize) + DATA_SIZE_PART + CODE_SIZE_PART; i++)
	{
		unsigned char save = data[i];
		int receiveCode = (int)save;
		dataIn.push_back((char)receiveCode);
	}
	return dataIn;
}
