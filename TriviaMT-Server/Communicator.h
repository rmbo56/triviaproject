#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <map>
#include <string>
#include <thread>

#include "LoginRequestHandler.h"
#include "IRequestHandler.h"
#include "ProtocolDATA.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "LoginRequestHandler.h"
#include "RequestHandlerFactory.h"


#define CODE_SIZE_PART 1
#define DATA_SIZE_PART 4

class Communicator
{
public:
	Communicator(RequestHandlerFactory& handlerFactory);
	void startHandleRequests();

private:
	SOCKET m_serverSocket;
	std::map<SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory &m_handlerFactory;

	void bindAndListen();
	void handleNewClient(SOCKET clientSocket);

	//handleNewClient helpers
	void sendData(SOCKET sc, const std::string message);
	char* getPartFromSocket(SOCKET sc, int flags);

	std::string bufferToString(Buffer buffer);
	char* recvFromClient(SOCKET sc, int size);

	int getDataSize(char* dataSize);
	Buffer charArrToBuffer(char* data);
};