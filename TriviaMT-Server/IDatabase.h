#pragma once
#include <string>
#include <iostream>
#include "json.hpp"
#include "Questions.h"
#include "User.h"
#include "Statistics.h"

class Questions;
class Statistics;

class IDatabase
{
public:
	virtual void addNewUser(std::string username, std::string password, std::string mail) = 0;
	virtual bool doesUserExists(std::string username) = 0;
	virtual bool doesPasswordMatch(std::string password, std::string userName) = 0;

	virtual std::list<Questions> getQuestions(int numOfquestion) = 0;

	virtual float getPlayerAverageAnswerTime(std::string user_name) = 0;
	virtual int getNumOfCorrectAnswers(std::string user_name) = 0;
	virtual int getNumOfTotalAnswers(std::string user_name) = 0;
	virtual int getNumOfPlayerGames(std::string user_name) = 0;
	virtual std::list<Statistics> getAllStatistics() = 0;///////

private:
};