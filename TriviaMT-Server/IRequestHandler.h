#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include "json.hpp"
using json = nlohmann::json;

typedef std::vector<unsigned char> Buffer;
class IRequestHandler;

enum RequestId
{
    LoginRequestID = 51,
    SignUpID = 52,
    LogoutRequestID = 53,
    JoinRoomRequestID = 54,
    CreateRoomRequestID = 55,
    GetRoomRequestID = 56,
    GetPlayersInRoomRequestID = 57,
    GetScoresStatisticsRequestID = 58,
    GetPalyerStatisticsRequestID = 59,
    RoomInfoRequestID = 60,
    closeRoomRequestID = 61,
    startGameRequestID = 62,
    getRoomStatRequestID = 63,
    leaveRoomRequestID = 64
};

//all the request that coming from the socket will get into this struct
struct RequestInfo
{
    RequestId id;
    time_t receivalTime;
    Buffer buffer;
};

struct RequestResult
{
    Buffer response;
    IRequestHandler* newHandler;
};

//sign up struct
struct SignupRequest
{
    std::string username;
    std::string password;
    std::string email;
};

//login struct
struct LoginRequest
{
    std::string username;
    std::string password;
};


//GetPlayersInRoom struct
struct GetPlayersInRoomRequest
{
    std::string roomName;
};

//JoinRoom struct
struct JoinRoomRequest
{
    std::string roomName;
};

struct RoomInfoRequest
{
    std::string roomName;
};
//CreateRoom struct
struct CreateRoomRequest
{
    std::string roomName;
    unsigned int maxUsers;
    unsigned int questionCount;
    unsigned int answerTimeout;
};


class IRequestHandler
{
public:
    virtual bool isRequestRelevant(RequestInfo info) = 0;
    virtual RequestResult handleRequest(RequestInfo info) = 0;


private:

};
