#include "JsonRequestPacketDeserializer.h"


//will create a LoginRequest from a Buffer
//input: Buffer
//output: LoginRequest
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(Buffer buffer)
{
    json json_saver = bufferToJsonData(buffer);//the buffer data as a json

    //making into struct of LoginRequest
    LoginRequest save_login;
    save_login.username = json_saver["username"];
    save_login.password = json_saver["password"];

    return save_login;
}


//will create a SignupRequest from a Buffer
//input: Buffer
//output: SignupRequest
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(Buffer buffer)
{
    json json_saver = bufferToJsonData(buffer);//the buffer data as a json

    //making into struct of SignupRequest
    SignupRequest save_signUp;
    save_signUp.username = json_saver["username"];
    save_signUp.password = json_saver["password"];
    save_signUp.email = json_saver["email"];

    return save_signUp;
}


//will take a buffer and return is data as json
json JsonRequestPacketDeserializer::bufferToJsonData(Buffer buffer)
{
    std::string save_string_part = "";
    int i = 0;
    //a loop that go over all the buffer from the 5 place (because we have 1 byte (the request code id) , 4 bytes( the length of the data))
    for (std::vector<unsigned char>::iterator it = buffer.begin(); it != buffer.end(); ++it)
    {
        if (i >= 5)
        {
            save_string_part += *it; //adding to the all string 
        }
        i++;
    }
    return json::parse(save_string_part); //make the string to json
}


//will create a GetPlayersInRoomRequest from a Buffer
//input: Buffer
//output: GetPlayersInRoomRequest
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(Buffer buffer)
{
    json json_saver = bufferToJsonData(buffer);//the buffer data as a json

     //making into struct of GetPlayersInRoomRequest
    GetPlayersInRoomRequest save_GetPlayersInRoomRequest;
    save_GetPlayersInRoomRequest.roomName = json_saver["roomName"];
   
    return save_GetPlayersInRoomRequest;
}

//will create a JoinRoomRequest from a Buffer
//input: Buffer
//output: JoinRoomRequest
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(Buffer buffer)
{
    json json_saver = bufferToJsonData(buffer);//the buffer data as a json

    //making into struct of GetPlayersInRoomRequest
    JoinRoomRequest save_JoinRoomRequest;
    save_JoinRoomRequest.roomName = json_saver["roomName"];

    return save_JoinRoomRequest;
}

//will create a CreateRoomRequest from a Buffer
//input: Buffer
//output: CreateRoomRequest
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(Buffer buffer)
{
    json json_saver = bufferToJsonData(buffer);//the buffer data as a json

    //making into struct of SignupRequest
    CreateRoomRequest save_CreateRoomRequest;
    save_CreateRoomRequest.answerTimeout = json_saver["answerTimeout"];
    save_CreateRoomRequest.maxUsers = json_saver["maxUsers"];
    save_CreateRoomRequest.questionCount = json_saver["questionCount"];
    save_CreateRoomRequest.roomName = json_saver["roomName"];

    return save_CreateRoomRequest;
}

RoomInfoRequest JsonRequestPacketDeserializer::deserializeRoomInfoRequest(Buffer buffer)
{
    json json_saver = bufferToJsonData(buffer);
    RoomInfoRequest save_RoomInfoRequest;
    save_RoomInfoRequest.roomName = json_saver["roomName"];

    return save_RoomInfoRequest;
}