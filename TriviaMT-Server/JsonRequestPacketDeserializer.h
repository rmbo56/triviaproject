#pragma once
#include <iostream>
#include <fstream>
#include <vector>
#include "LoginRequestHandler.h"

static class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(Buffer buffer);
	static SignupRequest deserializeSignupRequest(Buffer buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(Buffer buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(Buffer buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(Buffer buffer);
	static RoomInfoRequest deserializeRoomInfoRequest(Buffer buffer);

private:
	static json bufferToJsonData(Buffer buffer);
};