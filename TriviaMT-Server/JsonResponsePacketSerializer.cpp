#include "JsonResponsePacketSerializer.h"
void to_json(json& j, const RoomData& room);

//error serialization
//input:an error response
//output:a vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(ErrorResponse response)
{
	json responseJson;
	responseJson["message"] = response.message;
	return responseMSG(ERROR_CODE, responseJson.dump());
}

//login serialization
//input:an login response
//output:a vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(LoginResponse response)
{
	json responseJson;
	responseJson["status"] = response.status;
	return responseMSG(LOGIN_CODE, responseJson.dump());
}

//signup serialization
//input:an signup response
//output:a vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(SignupResponse response)
{
	json responseJson;
	responseJson["status"] = response.status;
	return responseMSG(SIGNUP_CODE, responseJson.dump());
}


//will make the Buffer msg
//input:the msg code by the protocol, the data of the msg
//output:a vector(Buffer) of the msg 
Buffer JsonResponsePacketSerializer::responseMSG(int msg_code, std::string data)
{
	unsigned char charMSG_CODE = msg_code;

	//will make the data length 4 bytes 
	std::string dataLengthSTR = "";
	unsigned char charDATA_LENGTH[4];
	charDATA_LENGTH[3] = (data.length() >> 24) & 0xFF;
	charDATA_LENGTH[2] = (data.length() >> 16) & 0xFF;
	charDATA_LENGTH[1] = (data.length() >> 8) & 0xFF;
	charDATA_LENGTH[0] = data.length() & 0xFF;
	for (int i = 3; i >= 0; i--)
	{
		dataLengthSTR += charDATA_LENGTH[i];
	}

	//will make the msg code 1 byte
	std::ostringstream oneBytesMSG_CODE;
	oneBytesMSG_CODE << std::setw(1) << std::setfill('0') << charMSG_CODE;
	
	//all the msg
	std::string msg = oneBytesMSG_CODE.str() + dataLengthSTR + data;
	//convert the string msg into a Buffer
	return Buffer(msg.begin(), msg.end());
}


//<don't need to call it>
//will enable the option to put a RoomData type vector in a json   
//input:the json, a RoomData    
//output:none
void to_json(json& j, const RoomData& room)
{
	j = { {"id", room.id},{"admin", room.admin}, {"name", room.name}, {"maxPlayers", room.maxPlayers}, {"numOfQuestionsInGame", room.numOfQuestionsInGame}, {"timePerQuestion", room.timePerQuestion}, {"isActive", room.isActive}};
}

//Logout serialization
//input:Logout response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(LogoutResponse response)
{
	json responseJson;
	response.status = 1;
	responseJson["status"] = response.status;
	return responseMSG(LOGOUT_CODE, responseJson.dump());
}

//GetRoom serialization
//input:GetRoom response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomResponse response)
{
	json responseJson;
	responseJson["rooms"] = response.rooms;
	return responseMSG(GET_ROOMS_CODE, responseJson.dump());
}

//GetPlayersInRoom serialization
//input:GetPlayersInRoom response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse response)
{
	json responseJson;
	responseJson["players"] = response.players;
	return responseMSG(GET_PLAYERS_IN_ROOM_CODE, responseJson.dump());
}

//JoinRoom serialization
//input:JoinRoom response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse response)
{
	json responseJson;
	response.status = 1;
	responseJson["status"] = response.status;
	return responseMSG(JOIN_ROOM_CODE, responseJson.dump());
}

//CreateRoom serialization
//input:CreateRoom response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse response)
{
	json responseJson;
	response.status = 1;
	responseJson["status"] = response.status;
	return responseMSG(CREATE_ROOM_CODE, responseJson.dump());
}	

//getHighScore serialization
//input:getHighScore response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(GetStatisticsResponse response)
{
	//GetStatisticsResponse
	json first_responseJson;
	first_responseJson["status"] = response.status;
	
	//highScores
	json second_responseJson;
	second_responseJson["status"] = response.highScores.status;
	second_responseJson["statistics"] = response.highScores.statistics;

	//Personal
	json third_responseJson;
	third_responseJson["status"] = response.statistics.status;
	third_responseJson["statistics"] = response.statistics.statistics;

	Buffer save_second= responseMSG(GET_HIGH_SCORE_RESPONSE, second_responseJson.dump()); //will save the high score
	Buffer save_third = responseMSG(GET_PERSONAL_CODE, third_responseJson.dump()); //will save the personal


	first_responseJson["highScores"] = save_second;
	first_responseJson["statistics"] = save_third;

	return responseMSG(GET_STATISTICS_CODE, first_responseJson.dump()); //will return all the parts 
}

//getPersonalStatsResponse serialization
//input:getPersonalStats response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(getPersonalStatsResponse response)
{
	json responseJson;
	//response.status = 1;
	//responseJson["status"] = response.status;
	//responseJson["statistics"] = response.statistics;

	for (auto i : response.statistics)
	{
		std::string curr = i;

		std::string delimiter = ":";
		std::string token = curr.substr(0, curr.find(delimiter));
		curr.erase(0, token.length() + delimiter.length());

		responseJson[token] = curr;
	}

	return responseMSG(GET_PERSONAL_CODE, responseJson.dump());
}

//getHighScoreResponse serialization
//input:getHighScore response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(getHighScoreResponse response)
{
	json responseJson;
	//response.status = 1;
	//responseJson["status"] = response.status;
	responseJson["statistics"] = response.statistics;

	return responseMSG(GET_HIGH_SCORE_RESPONSE, responseJson.dump());
}

Buffer JsonResponsePacketSerializer::serializeResponse(RoomInfoResponse response)
{
	json responseJson;
	to_json(responseJson, response.roomData);
	return responseMSG(GET_ROOM_INFO_CODE, responseJson.dump());
}


//CloseRoomResponse serialization
//input:CloseRoomResponse response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse response)
{
	json responseJson;
	response.status = 1;
	responseJson["status"] = response.status;
	
	return responseMSG(CLOSE_ROOM_CODE, responseJson.dump());
}

//StartGameResponse serialization
//input:StartGameResponse response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(StartGameResponse response)
{
	json responseJson;
	response.status = 1;
	responseJson["status"] = response.status;

	return responseMSG(START_GAME_CODE, responseJson.dump());
}

//GetRoomStateResponse serialization
//input:GetRoomStateResponse response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse response)
{
	json responseJson;

	responseJson["hasGameBegun"] = response.hasGameBegun;
	responseJson["players"] = response.players;
	responseJson["questionCount"] = response.questionCount;
	responseJson["answerTimeOut"] = response.answerTimeOut;

	return responseMSG(GET_ROOM_STATE_CODE, responseJson.dump());
}

//LeaveRoomResponse serialization
//input:LeaveRoomResponse response
//output:vector(Buffer) of the response
Buffer JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse response)
{
	json responseJson;
	response.status = 1;
	responseJson["status"] = response.status;

	return responseMSG(LEAVE_ROOM_CODE, responseJson.dump());
}
