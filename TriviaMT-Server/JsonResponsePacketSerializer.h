#pragma once
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include "json.hpp"
#include <sstream>
#include "ProtocolDATA.h"
#include "Statistics.h"

using json = nlohmann::json;
using Buffer = std::vector<unsigned char>;

static class JsonResponsePacketSerializer
{
public:
	static Buffer serializeResponse(ErrorResponse response);
	static Buffer serializeResponse(LoginResponse response);
	static Buffer serializeResponse(SignupResponse response);

	//new part
	static Buffer serializeResponse(LogoutResponse response);
	static Buffer serializeResponse(GetRoomResponse response);
	static Buffer serializeResponse(GetPlayersInRoomResponse response);
	static Buffer serializeResponse(JoinRoomResponse response);
	static Buffer serializeResponse(CreateRoomResponse response);
	static Buffer serializeResponse(GetStatisticsResponse response);

	//new
	static Buffer serializeResponse(getPersonalStatsResponse response);
	static Buffer serializeResponse(getHighScoreResponse response);
	static Buffer serializeResponse(RoomInfoResponse response);

	//new
	static Buffer serializeResponse(CloseRoomResponse response);
	static Buffer serializeResponse(StartGameResponse response);
	static Buffer serializeResponse(GetRoomStateResponse response);
	static Buffer serializeResponse(LeaveRoomResponse response);


private:
	static Buffer responseMSG(int msg_code, std::string data);
};