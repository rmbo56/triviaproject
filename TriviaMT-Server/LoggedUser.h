#pragma once
#include <string>

class LoggedUser
{
public:
	LoggedUser(std::string userName);
	std::string getUserName();

private:
	std::string m_userName;
};