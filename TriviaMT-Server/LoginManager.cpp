#include "LoginManager.h"
std::mutex loggedUsersAccess;
std::mutex databaseAccess;

LoginManager::LoginManager(IDatabase* database)
{
	std::unique_lock<std::mutex> locker(databaseAccess);
	m_database = database;
	locker.unlock();
}

//will do a sign up to a new user
//input: username, password, and mail
//output: none
void LoginManager::signup(std::string userName, std::string password, std::string mail)
{
	//if the user exists we can not create a new user with the same name
	if (m_database->doesUserExists(userName))
	{
		std::cout << "the username " << userName << " has already taken, try a new name" << std::endl;/////
		throw std::string("the username has already taken, try a new name");
	}
	else
	{
		m_database->addNewUser(userName, password, mail);
		std::cout << "Thank " << userName <<" for joining us!:)" << std::endl;
	}
	
}

//will do a login to exists user
//input: username, and password
//output: none
void LoginManager::login(std::string userName, std::string password)
{
	if (m_database->doesPasswordMatch(password, userName))//user is exists
	{	
		LoggedUser newUser(userName);
		if (userIsLoged(userName)) //already login
		{
			std::cout << "the user " << userName << " has already logged in" << std::endl;/////
			throw std::string("the user has already logged in");
		}
		else 
		{
			std::unique_lock<std::mutex> locker(databaseAccess);          //mutex
			m_loggedUsers.push_back(newUser);//will put the user in the connected users;
			locker.unlock();
		}
	}
	else
	{
		std::cout << "the user " << userName << " not exists" << std::endl;/////
		throw std::string("the user not exists");
	}
}

//will disconnect a user from the online user vecor
//input: the user username
//output: none
void LoginManager::logout(std::string userName)
{
	std::unique_lock<std::mutex> locker(loggedUsersAccess);              //mutex

	for (auto it = m_loggedUsers.begin(); it != m_loggedUsers.end(); ++it)
	{
		LoggedUser thisUser = *it;
		if (thisUser.getUserName() == userName)
		{
			m_loggedUsers.erase(it);
			break;
		}
	}
	locker.unlock();
}

//will check if a user is in the vector of connected users
//input: the username
//output: if he is in or not in the vector
bool LoginManager::userIsLoged(std::string userName)
{
	std::unique_lock<std::mutex> locker(loggedUsersAccess);

	for (auto it = m_loggedUsers.begin(); it != m_loggedUsers.end(); ++it)
	{
		LoggedUser thisUser = *it;
		if (thisUser.getUserName() == userName) {return true;}
	}
	locker.unlock();
	return false;
}