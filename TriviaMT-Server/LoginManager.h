#pragma once
#include "LoggedUser.h"
#include "IDatabase.h"
#include "SqliteDataBase.h"
#include <mutex>
#include <vector>

class IDatabase;
class LoggedUser;

class LoginManager
{
public:
	LoginManager(IDatabase* database);
	void signup(std::string userName, std::string password, std::string mail);
	void login(std::string userName, std::string password);
	void logout(std::string userName);


private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers;

	bool userIsLoged(std::string userName);
};