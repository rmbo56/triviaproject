#include "LoginRequestHandler.h"


LoginRequestHandler::LoginRequestHandler(LoginManager& m_loginManager, RequestHandlerFactory& _handlerFactory) :
    m_loginManager(m_loginManager), m_handlerFactory(_handlerFactory)
{
}

//a function that check if the code is a loginCode or SignUpCode 
bool LoginRequestHandler::isRequestRelevant(RequestInfo info)
{
    //check if the request is login or signUps
    if (info.buffer[0] == LoginRequestID || info.buffer[0] == SignUpID)
    {
        return true;
    }
    return false;
}

//a function that returns the answer after serialization 
RequestResult LoginRequestHandler::handleRequest(RequestInfo info)
{
    try
    {
        if (isRequestRelevant(info))
        {
            if ((int)info.buffer[0] == LoginRequestID)
            {
                //send him to the Login function
                return login(info);
            }
            else if ((int)info.buffer[0] == SignUpID)
            {
                //send him to the SignUp function
                return signUp(info);
            }
        }
        else
        {
           throw std::string ("an error was found in the login or sign up msg"); //return null
        }
    }
    catch (std::string caught)//error
    {
        ErrorResponse saveErrorResponse;
        saveErrorResponse.message = caught;
        Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveErrorResponse); //doing serialize

        RequestResult saveRequestResult;
        saveRequestResult.response = saveBuf;
        saveRequestResult.newHandler = this; // m_handlerFactory.createLoginRequestHandler(); //new LoginRequestHandler(m_loginManager);///////////
        return saveRequestResult;
    }
}

//login
RequestResult LoginRequestHandler::login(RequestInfo info)
{
    LoginRequest saveLoginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(info.buffer); //doing deserialize
    m_loginManager.login(saveLoginRequest.username,saveLoginRequest.password); //joining the loginManager-> login
    //The response
    LoginResponse saveLoginResponse;
    saveLoginResponse.status = 1;

    Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveLoginResponse); //doing serialize

    LoggedUser userName(saveLoginRequest.username);
    //All the result
    RequestResult saveRequestResult;
    saveRequestResult.response=saveBuf;
    saveRequestResult.newHandler = m_handlerFactory.createMenuRequestHandler(userName);
    
    return saveRequestResult;
}

//signUp
RequestResult LoginRequestHandler::signUp(RequestInfo info)
{
    SignupRequest saveSignupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(info.buffer); //doing deserialize
    m_loginManager.signup(saveSignupRequest.username, saveSignupRequest.password, saveSignupRequest.email); //joining the loginManager -> signUp
    SignupResponse saveSignupResponse;
    saveSignupResponse.status = 1;

    Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveSignupResponse);//doing serialize

    //All the result
    RequestResult saveRequestResult;
    saveRequestResult.response=saveBuf;
    saveRequestResult.newHandler = this; // m_handlerFactory.createLoginRequestHandler();  //new LoginRequestHandler(m_loginManager);
    return saveRequestResult;
}
