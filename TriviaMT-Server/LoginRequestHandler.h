#pragma once
#include "IRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "LoginManager.h"

#include "MenuRequestHandler.h"
#include "RequestHandlerFactory.h"

class IRequestHandler;
//class LoginRequestHandler;
class LoginManager;
class RequestHandlerFactory;
class RequestResult;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(LoginManager& m_loginManager, RequestHandlerFactory& _handlerFactory);

	bool isRequestRelevant(RequestInfo info) override;
	RequestResult handleRequest(RequestInfo info) override;


private:
	RequestResult login(RequestInfo info);
	RequestResult signUp(RequestInfo info);

	LoginManager& m_loginManager;
	RequestHandlerFactory& m_handlerFactory;
};