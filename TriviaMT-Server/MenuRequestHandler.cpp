#include "MenuRequestHandler.h"

MenuRequestHandler::MenuRequestHandler(LoggedUser user, RequestHandlerFactory& handlerFactory) :
	_user(user), _roomManager(handlerFactory.getRoomManager()), _statisticManager(handlerFactory.getStatisticsManager()), _handlerFactory(handlerFactory)
{

}

bool MenuRequestHandler::isRequestRelevant(RequestInfo info)
{
	if (info.buffer[0] >= LogoutRequestID && info.buffer[0] <= RoomInfoRequestID)// if request's code is exists in the protocol
	{
		return true;
	}

	return false;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo info)
{
	try
	{
		if (isRequestRelevant(info))
		{
			if(info.buffer[0] == LogoutRequestID) {return signout(info);}
			else if (info.buffer[0] == GetRoomRequestID) {return getRooms(info);}
			else if (info.buffer[0] == GetPlayersInRoomRequestID) {return getPlayersInRoom(info);}
			else if (info.buffer[0] == GetPalyerStatisticsRequestID) {return getPersonalStats(info);}
			else if (info.buffer[0] == GetScoresStatisticsRequestID) {return getHighScore(info);}
			else if (info.buffer[0] == JoinRoomRequestID) {return joinRoom(info);}
			else if (info.buffer[0] == CreateRoomRequestID) { return createRoom(info);}
			else if (info.buffer[0] == RoomInfoRequestID ){ return getRoomInfo(info);}
		}
		else
		{
			throw std::string("an error was found in the menu msg");
		}
	}
	catch (std::string caught)//error
	{
		ErrorResponse saveErrorResponse;
		saveErrorResponse.message = caught;
		Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveErrorResponse); //doing serialize

		RequestResult saveRequestResult;
		saveRequestResult.response = saveBuf;
		saveRequestResult.newHandler = this;
		return saveRequestResult;
	}
}

RequestResult MenuRequestHandler::signout(RequestInfo info)
{
	//The response
	LogoutResponse saveResponse;
	saveResponse.status = LOGOUT_STATUS;

	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize
	_handlerFactory.getLoginManager().logout(_user.getUserName());
	//All the result
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = nullptr;
	return saveRequestResult;
}

RequestResult MenuRequestHandler::getRooms(RequestInfo info)
{
	GetRoomResponse saveResponse;
	saveResponse.status = GETROOM_STATUS;

	std::vector<Room> saveRoomType = _roomManager.getRooms();

	for (auto room : saveRoomType)
	{
		saveResponse.rooms.push_back(room.getRoomData());
	}

	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse);

	//All the result
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = _handlerFactory.createMenuRequestHandler(_user);
	return saveRequestResult;

}

RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo info)
{
	GetPlayersInRoomRequest saveRequest = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(info.buffer);//doing deserialize
	GetPlayersInRoomResponse saveResponse;
	std::vector<Room> saveRoomType = _roomManager.getRooms();

	for (auto room : saveRoomType)
	{
		RoomData roomData = room.getRoomData();
		if (roomData.name == saveRequest.roomName)
		{
			std::vector<LoggedUser> allUsers = room.getAllUsers();
			for (auto user : allUsers)
			{
				saveResponse.players.push_back(user.getUserName());
			}
		}
	}
	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize

	//All the result
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = _handlerFactory.createMenuRequestHandler(_user);
	return saveRequestResult;
}

RequestResult MenuRequestHandler::getPersonalStats(RequestInfo info)
{
	getPersonalStatsResponse saveResponse;
	saveResponse.status = 1;

	saveResponse.statistics = _statisticManager.getUserStatistics(_user.getUserName());

	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize

	//All the result
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = _handlerFactory.createMenuRequestHandler(_user);
	return saveRequestResult;
}

RequestResult MenuRequestHandler::getHighScore(RequestInfo info)
{
	getHighScoreResponse saveResponse;
	saveResponse.status = 1;
	saveResponse.statistics = _statisticManager.getHighScore();


	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize

	//All the result
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = _handlerFactory.createMenuRequestHandler(_user);
	return saveRequestResult;
}

RequestResult MenuRequestHandler::joinRoom(RequestInfo info)
{
	JoinRoomRequest saveRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(info.buffer); //doing deserialize;
	
	//will add the player to the room by the room id
	_roomManager.insertUserToRoom(saveRequest.roomName, _user.getUserName());

	JoinRoomResponse saveResponse;
	saveResponse.status = 1;
	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize

	//the result
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = _handlerFactory.createMenuRequestHandler(_user);
	return saveRequestResult;
}

//need to add the room to the project
RequestResult MenuRequestHandler::createRoom(RequestInfo info)
{
	CreateRoomRequest saveRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(info.buffer); //doing deserialize;
	RoomData newRoomData;
	newRoomData.admin = _user.getUserName();
	newRoomData.id = CreateNewRoomID();
	newRoomData.isActive = false;
	newRoomData.maxPlayers = saveRequest.maxUsers;
	newRoomData.name = saveRequest.roomName;
	newRoomData.numOfQuestionsInGame = saveRequest.questionCount;
	newRoomData.timePerQuestion = saveRequest.answerTimeout;

	//will add the room to the rooms list
	this->_roomManager.createRoom(_user, newRoomData);

	JoinRoomResponse saveResponse;
	saveResponse.status = 1;
	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize

	//the result
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = _handlerFactory.createMenuRequestHandler(_user);// will go to the RoomAdminRequestHandler
	return saveRequestResult;
}

//will create a new ID for a new Room
//input: none
//output: none
int MenuRequestHandler::CreateNewRoomID()
{
	std::vector<Room> saveRoomType = _roomManager.getRooms();

	int id = 0;
	int have = 2;
	bool in = false;
	while (have % 2 == 0)//mark 1 need to and the loop
	{
		id++;
		have = 2;
		for (auto room : saveRoomType)//will check if there is a room with this id
		{
			in = true;
			RoomData roomData = room.getRoomData();
			if (roomData.id == id)//if a room with the same id was found, will mark it with 4 
			{
				have = 4;
			}
			if(roomData.id != id && have != 4)//if a room dont have the found mark and have a different id will mark with 1
			{
				have = 1;
			}
		}
		if (!in)//If the program does not go to the for of all the rooms it means there are no rooms
		{
			break;
		}
	}
	return id;
}


RequestResult MenuRequestHandler::getRoomInfo(RequestInfo info)
{
	RoomInfoResponse saveRoomInfoResponse;
	std::string roomName = "";

	for (int i = 0; i < info.buffer.size(); i++)//save buffer as a string
	{
		roomName += info.buffer[i];
	}
	RoomInfoRequest roomInfo = JsonRequestPacketDeserializer::deserializeRoomInfoRequest(info.buffer);
	saveRoomInfoResponse.roomData = _roomManager.getRoomByName(roomInfo.roomName).getRoomData();//gets room information
	Buffer responseBuff = JsonResponsePacketSerializer::serializeResponse(saveRoomInfoResponse);//serialize room data into json

	RequestResult saveRequestResult;
	saveRequestResult.response = responseBuff;
	saveRequestResult.newHandler = _handlerFactory.createMenuRequestHandler(_user);

	return saveRequestResult;
}