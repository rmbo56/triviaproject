#pragma once
#include "RoomManager.h"
#define SERVER_PORT 5566

#define ERROR_CODE 100
#define LOGIN_CODE 101
#define SIGNUP_CODE 102
#define LOGOUT_CODE 103
#define JOIN_ROOM_CODE 104
#define CREATE_ROOM_CODE 105
#define GET_ROOMS_CODE 106
#define GET_PLAYERS_IN_ROOM_CODE 107
#define GET_HIGH_SCORE_RESPONSE 108
#define GET_PERSONAL_CODE 109
#define GET_STATISTICS_CODE 110
#define GET_ROOM_INFO_CODE 111

#define CLOSE_ROOM_CODE 112
#define START_GAME_CODE 113
#define LEAVE_ROOM_CODE 114
#define GET_ROOM_STATE_CODE 115


#define LOGIN_STATUS 1
#define SIGNUP_STATUS 2
#define LOGOUT_STATUS 3
#define GETROOM_STATUS 4

struct LoginResponse
{
    unsigned int status;
};

struct SignupResponse
{
    unsigned int status;
};

struct ErrorResponse
{
    std::string message;
};

struct LogoutResponse
{
    unsigned int status;
};

struct GetRoomResponse
{
    unsigned int status;
    std::vector<RoomData> rooms;
};

struct GetPlayersInRoomResponse
{
    std::vector<std::string> players; 
};

struct getHighScoreResponse
{
    unsigned int status;
    std::vector<std::string> statistics;
};

struct getPersonalStatsResponse
{
    unsigned int status;
    std::vector<std::string> statistics;
};

struct GetStatisticsResponse
{
    unsigned int status;
    getHighScoreResponse highScores;
    getPersonalStatsResponse statistics;
};

struct JoinRoomResponse
{
    unsigned int status;
};
struct CreateRoomResponse
{
    unsigned int status;
};

struct RoomInfoResponse
{
    RoomData roomData;
};

struct CloseRoomResponse
{
    unsigned int status;
};

struct StartGameResponse
{
    unsigned int status;
};

struct GetRoomStateResponse
{
    bool hasGameBegun;
    std::vector<std::string> players;
    unsigned int questionCount;
    unsigned int answerTimeOut;
};

struct LeaveRoomResponse
{
    unsigned int status;
};