#include "Questions.h"


Questions::Questions(const std::string& question, const std::string& answer1, const std::string& answer2, const std::string& answer3, const std::string& answer4)
{
	_question = question;
	_answer1 = answer1;
	_answer2 = answer2;
	_answer3 = answer3;
	_answer4 = answer4;
}

const std::string& Questions::getQuestions() const
{
	return _question;
}

void Questions::setQuestions(const std::string& questions)
{
	_question = questions;
}

const std::string& Questions::getCorrectQuestion() const
{
	return _answer1;
}

void Questions::setCorrectQuestion(const std::string& answer)
{
	_answer1 = answer;
}

const std::string& Questions::getanswer2() const
{
	return _answer2;
}

void Questions::setanswer2(const std::string& answer)
{
	_answer2 = answer;
}

const std::string& Questions::getanswer3() const
{
	return _answer3;
}

void Questions::setanswer3(const std::string& answer)
{
	_answer3 = answer;
}

const std::string& Questions::getanswer4() const
{
	return _answer4;
}

void Questions::setanswer4(const std::string& answer)
{
	_answer4 = answer;
}

bool Questions::operator==(const Questions& other) const
{
	return this->_question == other._question;
}

bool Questions::operator==(std::string question) const
{
	return this->_question == question;
}
