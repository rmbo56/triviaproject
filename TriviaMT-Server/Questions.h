#pragma once
#include <string>
#include <iostream>
#include <list>

class Questions;
typedef std::list<Questions>::iterator questions_iter;

class Questions
{
public:

	Questions(const std::string& question, const std::string& answer1, const std::string& answer2, const std::string& answer3, const std::string& answer4);

	const std::string& getQuestions() const;
	void setQuestions(const std::string& questions);

	const std::string& getCorrectQuestion() const;
	void setCorrectQuestion(const std::string& answer);

	const std::string& getanswer2() const;
	void setanswer2(const std::string& answer);

	const std::string& getanswer3() const;
	void setanswer3(const std::string& answer);

	const std::string& getanswer4() const;
	void setanswer4(const std::string& answer);

	bool operator==(const Questions& other) const;
	bool operator==(std::string question) const;
private:
	std::string _question;
	std::string _answer1;//the correct answer
	std::string _answer2;
	std::string _answer3;
	std::string _answer4;
};
