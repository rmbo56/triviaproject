#include "RequestHandlerFactory.h"


RequestHandlerFactory::RequestHandlerFactory(IDatabase* database):
    m_database(database), m_loginManager(m_database), m_statisticsManager(m_database), m_roomManager()
{
}

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
    LoginRequestHandler* loginRequest = new LoginRequestHandler(m_loginManager, *this);
    return loginRequest;
}

//will get the LoginManager
LoginManager& RequestHandlerFactory::getLoginManager()
{
    return m_loginManager;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
    return m_statisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
    return m_roomManager;
}


MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
    return new MenuRequestHandler(user, *this);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, RequestHandlerFactory& handlerFactory)
{
    return new RoomAdminRequestHandler(user, *this, m_roomManager.getRoomIDbyUsername(user.getUserName()));
}

RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, RequestHandlerFactory& handlerFactory)
{
    return new RoomMemberRequestHandler(user, *this, m_roomManager.getRoomIDbyUsername(user.getUserName()));
}
