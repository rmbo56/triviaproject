#pragma once
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "IDatabase.h"
#include "LoginManager.h"
#include "SqliteDataBase.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "MenuRequestHandler.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"

#include <mutex>


class IDatabase;
class LoggedUser;
class LoginManager;
class LoginRequestHandler;
class StatisticsManager;
class RoomManager;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;

class RequestHandlerFactory	
{
public:
	RequestHandlerFactory(IDatabase* database);

	LoginRequestHandler* createLoginRequestHandler();
	LoginManager& getLoginManager();

	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser user);

	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser user, RequestHandlerFactory& handlerFactory);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser user, RequestHandlerFactory& handlerFactory);
private:
	IDatabase* m_database;
	LoginManager m_loginManager;
	StatisticsManager m_statisticsManager;
	RoomManager m_roomManager;
};