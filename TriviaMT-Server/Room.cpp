#include "Room.h"

Room::Room(const RoomData data)
{
	m_metadata = data;
}

Room::~Room()
{

}

void Room::addUser(LoggedUser user)
{
	bool alreadyExist = false;
	//checks if user already connected
	for (auto it = m_users.begin(); it != m_users.end(); it++)
	{
		if (it->getUserName() == user.getUserName())
		{
			alreadyExist = true;
		}
	}

	if (!alreadyExist)
	{
		m_users.push_back(user);//push user into the room
	}
	else
	{
		throw std::string("User is already connected to the room");
	}
}

void Room::removeUser(LoggedUser user)
{
	bool alreadyExist = false;
	int i = 0;
	int index;

	for (auto it = m_users.begin(); it != m_users.end(); i++, it++)
	{
		if (it->getUserName() == user.getUserName())
		{
			m_users.erase(it);
			alreadyExist = true;
		}
	}

	if (!alreadyExist)
	{
		throw std::string("User doesn't exist");
	}
}


std::vector<LoggedUser> Room::getAllUsers() const
{
	return m_users;
}

RoomData Room::getRoomData() const 
{
	return m_metadata;
}