#pragma once
#include "LoggedUser.h"
#include <vector>
#include<list>

struct RoomData
{
	std::string admin;
	unsigned int id;
	std::string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionsInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;
};

class Room
{
private:
	std::vector<LoggedUser> m_users;
	RoomData m_metadata;
	
public:
	Room(const RoomData data);
	Room() = default;
	~Room();
	void addUser(LoggedUser user);
	void removeUser(LoggedUser user);
	std::vector<LoggedUser> getAllUsers() const;
	RoomData getRoomData()const;
};