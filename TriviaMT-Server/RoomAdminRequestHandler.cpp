#include "RoomAdminRequestHandler.h"

RoomAdminRequestHandler::RoomAdminRequestHandler(LoggedUser user, RequestHandlerFactory& handlerFactory, int roomID) :
	_user(user), _handlerFactory(handlerFactory), _roomManager(handlerFactory.getRoomManager()), _roomID(roomID)
{
}

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo info)
{
	if (info.buffer[0] >= closeRoomRequestID && info.buffer[0] <= getRoomStatRequestID)// if request's code is exists in the protocol
	{
		return true;
	}

	return false;
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo info)
{
	try
	{
		if (isRequestRelevant(info))
		{
			if (info.buffer[0] == closeRoomRequestID) { return closeRoom(info); }
			else if (info.buffer[0] == startGameRequestID) { return startGame(info); }
			else if (info.buffer[0] == getRoomStatRequestID) { return getRoomState(info); }
		}
		else
		{
			throw std::string("an error was found in the menu msg");
		}
	}
	catch (std::string caught)//error
	{
		ErrorResponse saveErrorResponse;
		saveErrorResponse.message = caught;
		Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveErrorResponse); //doing serialize

		RequestResult saveRequestResult;
		saveRequestResult.response = saveBuf;
		saveRequestResult.newHandler = this;
		return saveRequestResult;
	}
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo info)
{
	//need to add leaveGameRoomResponse to all the players
	_roomManager.deleteRoom(_roomID);

	CloseRoomResponse saveResponse;
	saveResponse.status = 1;

	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = _handlerFactory.createMenuRequestHandler(_user);//will go back to the menu;



	return saveRequestResult;
}

RequestResult RoomAdminRequestHandler::startGame(RequestInfo info)
{
	_roomManager.setRoomActivity(_roomID, true);

	StartGameResponse saveResponse;
	saveResponse.status = 1;

	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = this;//  ??? _handlerFactory.createMenuRequestHandler(_user);

	//need to add startGameRoomResponse to all the players

	return saveRequestResult;
}

RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo info)
{
	GetRoomStateResponse saveResponse;

	RoomData roomdata = _roomManager.getRoomData(_roomID);
	saveResponse.answerTimeOut = roomdata.timePerQuestion;
	saveResponse.hasGameBegun = roomdata.isActive;
	saveResponse.players = _roomManager.getPlayersInRoom(_roomID);
	saveResponse.questionCount = roomdata.numOfQuestionsInGame;

	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = this;

	return saveRequestResult;
}
