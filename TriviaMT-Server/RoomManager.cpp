#include "RoomManager.h"
std::mutex roomDataAccess;

void RoomManager::createRoom(const LoggedUser creator, const RoomData data)
{
	Room r(data);
	r.addUser(creator);
	std::unique_lock<std::mutex> locker(roomDataAccess);     //mutex
	m_rooms.insert(std::pair<unsigned int, Room>(data.id, r));
	locker.unlock();
}

void RoomManager::deleteRoom(const unsigned int roomId)
{
	std::lock_guard<std::mutex> locker(roomDataAccess);    //mutex

	if (m_rooms.find(roomId) == m_rooms.end())
	{
		throw std::exception("Room with the entered id doesn't exist");
	}

	m_rooms.erase(roomId);
	
}

unsigned int RoomManager::getRoomState(const int ID) const
{
	std::lock_guard<std::mutex> locker(roomDataAccess);
	if (m_rooms.find(ID) == m_rooms.end())
	{
		throw std::exception("Room with the entered id doesn't exist");
	}

	Room r = m_rooms.find(ID)->second;
	

	return r.getRoomData().isActive;
}

std::vector<Room> RoomManager::getRooms() const
{
	std::vector<Room> v;
	std::unique_lock<std::mutex> locker(roomDataAccess);
	for (auto it = m_rooms.begin(); it != m_rooms.end(); it++)
	{
		v.push_back(it->second);
	}
	locker.unlock();
	return v;
}

Room RoomManager::getRoomByName(std::string name)
{
	for (auto it = m_rooms.begin(); it != m_rooms.end(); it++)
	{
		if (it->second.getRoomData().name == name)
		{
			return it->second;
		}
	}
	throw "Room has not found!";
}

int RoomManager::getRoomIDbyUsername(std::string name)
{
	for (auto room : m_rooms)
	{
		std::vector<LoggedUser> userInRoom = room.second.getAllUsers();
		for(auto user : userInRoom)
		{
			if (user.getUserName() == name)
			{
				return room.first;//the room ID
			}
		}
	}
	return 0;
}

void RoomManager::setRoomActivity(int roomID, bool activity)
{
	std::map<unsigned int, Room>::iterator it;
	it = m_rooms.find(roomID);
	if(it == m_rooms.end())
	{
		throw std::string("can't find room");
	}
	Room& room = m_rooms[roomID];

	RoomData roomData = room.getRoomData();
	if (activity) { roomData.isActive = 1; }
	else { roomData.isActive = 0; }
	room = Room(roomData);
}

RoomData RoomManager::getRoomData(int roomID)
{
	std::map<unsigned int, Room>::iterator it;
	it = m_rooms.find(roomID);
	if (it == m_rooms.end())
	{
		throw std::string("can't find room");
	}
	return m_rooms[roomID].getRoomData();
}

std::vector<std::string> RoomManager::getPlayersInRoom(int roomID)
{
	std::vector<std::string> allUsers;

	std::map<unsigned int, Room>::iterator it;
	it = m_rooms.find(roomID);
	if (it == m_rooms.end())
	{
		throw std::string("can't find room");
	}

	for (auto user : m_rooms[roomID].getAllUsers())
	{
		allUsers.push_back(user.getUserName());
	}
	return allUsers;
}

void RoomManager::removeUSerFromRoom(int roomID, LoggedUser user)
{
	std::map<unsigned int, Room>::iterator it;
	it = m_rooms.find(roomID);
	if (it == m_rooms.end())
	{
		throw std::string("can't find room");
	}

	Room& room = m_rooms[roomID];
	room.addUser(user);
}

//a fucntion that will insert the user to the room
void RoomManager::insertUserToRoom(std::string roomName, std::string userName)
{
	bool exist = false;
	//will add the player to the room by the room id
	for (auto &room : m_rooms)
	{
		RoomData roomData = room.second.getRoomData();

		if (roomData.name == roomName)
		{
			exist = true;
			room.second.addUser(userName);
		}
	}
	if (!exist)
	{
		throw std::string("can't find room");
	}
}
