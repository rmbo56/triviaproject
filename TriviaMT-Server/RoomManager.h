#pragma once
#include "Room.h"
#include <map>
#include <mutex>

class Room;
class LoggedUser;

class RoomManager
{
private:
	std::map<unsigned int, Room> m_rooms;
	

public:
	//c'tor, d'tor
	RoomManager() = default;
	~RoomManager() = default;

	//managing rooms
	void createRoom(const LoggedUser creator, const RoomData data);
	void deleteRoom(const unsigned int roomId);

	//rooms info
	unsigned int getRoomState(const int ID) const;
	std::vector<Room> getRooms() const;
	Room getRoomByName(std::string name);
	int getRoomIDbyUsername(std::string name);
	void setRoomActivity(int roomID, bool activity);
	RoomData getRoomData(int roomID);
	std::vector<std::string> getPlayersInRoom(int roomID);
	void removeUSerFromRoom(int roomID, LoggedUser user);

	void insertUserToRoom(std::string roomName, std::string userName);
	
};

