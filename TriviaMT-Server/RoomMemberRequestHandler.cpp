#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(LoggedUser user, RequestHandlerFactory& handlerFactory, int roomID) :
	_user(user), _handlerFactory(handlerFactory), _roomManager(handlerFactory.getRoomManager()), _roomID(roomID)
{

}

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo info)
{
	if (info.buffer[0] >= getRoomStatRequestID && info.buffer[0] <= leaveRoomRequestID)// if request's code is exists in the protocol
	{
		return true;
	}
	return false;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo info)
{
	try
	{
		if (isRequestRelevant(info))
		{
			if (info.buffer[0] == leaveRoomRequestID) { return leaveRoom(info); }
			else if (info.buffer[0] == getRoomStatRequestID) { return getRoomState(info); }
		}
		else
		{
			throw std::string("an error was found in the menu msg");
		}
	}
	catch (std::string caught)//error
	{
		ErrorResponse saveErrorResponse;
		saveErrorResponse.message = caught;
		Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveErrorResponse); //doing serialize

		RequestResult saveRequestResult;
		saveRequestResult.response = saveBuf;
		saveRequestResult.newHandler = this;
		return saveRequestResult;
	}
}

RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo info)
{
	_roomManager.removeUSerFromRoom(_roomID, _user);

	LeaveRoomResponse saveResponse;
	saveResponse.status = 1;

	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = this;

	return saveRequestResult;
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo info)
{
	GetRoomStateResponse saveResponse;

	RoomData roomdata = _roomManager.getRoomData(_roomID);
	saveResponse.answerTimeOut = roomdata.timePerQuestion;
	saveResponse.hasGameBegun = roomdata.isActive;
	saveResponse.players = _roomManager.getPlayersInRoom(_roomID);
	saveResponse.questionCount = roomdata.numOfQuestionsInGame;

	Buffer saveBuf = JsonResponsePacketSerializer::serializeResponse(saveResponse); //doing serialize
	RequestResult saveRequestResult;
	saveRequestResult.response = saveBuf;
	saveRequestResult.newHandler = this;

	return saveRequestResult;
}
