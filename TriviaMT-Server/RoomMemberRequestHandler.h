#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
class IRequestHandler;
class RequestResult;
class LoggedUser;
class RoomManager;
class StatisticsManager;
class RequestHandlerFactory;

class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(LoggedUser user, RequestHandlerFactory& handlerFactory, int roomID);

	bool isRequestRelevant(RequestInfo info) override;
	RequestResult handleRequest(RequestInfo info) override;

private:
	LoggedUser _user;
	RequestHandlerFactory& _handlerFactory;
	RoomManager& _roomManager;
	int _roomID;

	RequestResult leaveRoom(RequestInfo info);
	RequestResult getRoomState(RequestInfo info);
};