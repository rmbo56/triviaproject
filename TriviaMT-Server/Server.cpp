#include "Server.h"


Server::Server() :
	m_database(new SqliteDataBase()), m_handlerFactory(RequestHandlerFactory(m_database)), m_communicator(m_handlerFactory)
{
}

//will give the user an <EXIT> option and will send to the user login
void Server::run()
{
	//Creates a thread that will do the clients connect
	std::thread t_connector(&Communicator::startHandleRequests, &m_communicator);
	t_connector.detach();
	std::cout << "if you want to exit the program type <EXIT>" << std::endl;
	this->exitProgram();
}

//will wait for "EXIT" string.
//if will get "EXIT" the program will stop
void Server::exitProgram()
{
	std::string exitCode;
	do
	{
		std::cin >> exitCode;
	} while (exitCode != "EXIT");
	exit(-1);//will close the program
}