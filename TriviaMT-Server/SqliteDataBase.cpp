#include "SqliteDataBase.h"
std::mutex dbAccess;

std::list<User> userList;//list of users
std::list<Questions> questionList;//list of question
std::list<Statistics> statisticsList;//list of statistics

SqliteDataBase::SqliteDataBase()
{
	std::lock_guard<std::mutex> locker(dbAccess);         //mutex
	open();
}

SqliteDataBase::~SqliteDataBase()
{
	std::lock_guard<std::mutex> locker(dbAccess);         //mutex
	sqlite3_close(db);
	db = nullptr;
}

//will add a user to the DB
//input: the msg from the client
//output: none
void SqliteDataBase::addNewUser(std::string username, std::string password, std::string mail)
{
	//will make the code that need to be sand to the DB
	std::string code = ADD_USER;
	std::string replace_code = USER_NAME;
	code.replace(code.find(replace_code), replace_code.length(), username);

	replace_code = USER_PASSWORD;
	code.replace(code.find(replace_code), replace_code.length(), password);

	replace_code = USER_MAIL;
	code.replace(code.find(replace_code), replace_code.length(), mail);

	//will put the msg with the user in the user table
	const char* sqlStatement = code.c_str();
	char* errMessage = nullptr;
	std::lock_guard<std::mutex> locker(dbAccess);         //mutex
	int res = sqlite3_exec(this->db, sqlStatement, nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::invalid_argument("Failed to add the user [" + username + "]" + "\n");
	}
}

//will check if a user exists
//input: user name
//output: if exists or not
bool SqliteDataBase::doesUserExists(std::string username)
{
	std::string code = DOES_USER_EXISTS_BY_USER_NAME;
	std::string replace_code = USER_NAME;
	code.replace(code.find(replace_code), replace_code.length(), username);

	const char* sqlStatement = code.c_str();
	char* errMessage = nullptr;
	std::lock_guard<std::mutex> locker(dbAccess);         //mutex
	userList.clear();
	int res = sqlite3_exec(db, sqlStatement, this->callback_users, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::invalid_argument("Failed to get user by name\n");
	}
	if (!userList.empty())
	{
		return true;
	}
	return false;
}

//will check if a user exists
//input: password, user name
//output: if exists or not
bool SqliteDataBase::doesPasswordMatch(std::string password, std::string userName)
{
	std::string code = DOES_USER_EXISTS_BY_USER_NAME_AND_PASSWORD;
	std::string replace_code = USER_NAME;
	code.replace(code.find(replace_code), replace_code.length(), userName);

	replace_code = USER_PASSWORD;
	code.replace(code.find(replace_code), replace_code.length(), password);

	const char* sqlStatement = code.c_str();
	char* errMessage = nullptr;
	std::lock_guard<std::mutex> locker(dbAccess);         //mutex
	userList.clear();
	int res = sqlite3_exec(db, sqlStatement, this->callback_users, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::invalid_argument("Failed to get user by password and name\n");
	}
	if (!userList.empty())
	{
		return true;
	}
	return false;
}

//will give questions from the DB based of the number 
//input: number of question
//output: list of question 
std::list<Questions> SqliteDataBase::getQuestions(int numOfquestion)
{
	std::string code = GET_QEUSTIONS;

	const char* sqlStatement = code.c_str();
	char* errMessage = nullptr;
	std::lock_guard<std::mutex> locker(dbAccess);         //mutex
	questionList.clear();
	int res = sqlite3_exec(db, sqlStatement, this->callback_questions, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::invalid_argument("Failed to get user by password and name\n");
	}
	std::list<Questions> saveAmountOfQuestions;

	int correntCount = 0;
	for (auto const& i : questionList)
	{
		saveAmountOfQuestions.push_back(i);
		correntCount++;
		if (correntCount == numOfquestion) { break; }
	}

	return saveAmountOfQuestions;
}


float SqliteDataBase::getPlayerAverageAnswerTime(std::string userName)
{
	getStatistics(userName);
	
	Statistics statistics = statisticsList.back();//need to be only one

	return (statistics.getPalyTime() / statistics.getTotal());
}

int SqliteDataBase::getNumOfCorrectAnswers(std::string userName)
{
	getStatistics(userName);

	Statistics statistics = statisticsList.back();//need to be only one

	return statistics.getCorrect();
}

int SqliteDataBase::getNumOfTotalAnswers(std::string userName)
{
	getStatistics(userName);

	Statistics statistics = statisticsList.back();//need to be only one

	return statistics.getTotal();
}

int SqliteDataBase::getNumOfPlayerGames(std::string userName)
{
	getStatistics(userName);

	Statistics statistics = statisticsList.back();//need to be only one

	return statistics.getGames();
}

std::list<Statistics> SqliteDataBase::getAllStatistics()
{
	getStatistics("%%");//%% = all the users
	return statisticsList;
}

//will open/create the DB and the tables
//input: none
//output: open/create successfully or not
bool SqliteDataBase::open()
{
	std::string dbFileName = DB_FILE_NAME;
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &this->db);
	if (res != SQLITE_OK)
	{
		this->db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	//if the db not exists will make his tables
	if (doesFileExist != 0) 
	{
		bool ans = this->create_table(USERS_CREATE);
		if (!ans)
		{
			return false;
		}
		ans = this->create_table(QUESTIONS_CREATE);
		if (!ans)
		{
			return false;
		}
		ans = this->create_table(STATISTICS_CREATE);
		if (!ans)
		{
			return false;
		}
	}
	return true;
}

//the function will be in use from getting info from DB from users table, will put the data in a list of users
//input:?
//output: 0
int SqliteDataBase::callback_users(void* data, int argc, char** argv, char** azColName)
{
	User userDB("none", "none", "none");
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "PASSWORD") {
			userDB.setPassword(argv[i]);
		}
		else if (std::string(azColName[i]) == "NAME") {
			userDB.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "MAIL") {
			userDB.setMail(argv[i]);
		}
	}
	if (userDB.getMail() != "none" && userDB.getName() != "none" && userDB.getPassword() != "none")
	{
		userList.push_back(userDB);
	}

	return 0;

}

//the function will be in use from getting info from DB from questions table, will put the data in a list of questions
//input:?
//output: 0
int SqliteDataBase::callback_questions(void* data, int argc, char** argv, char** azColName)
{
	Questions questionDB("none", "none", "none", "none", "none");
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "QUESTION") {
			questionDB.setQuestions(argv[i]);
		}
		else if (std::string(azColName[i]) == "ANSWER1") {
			questionDB.setCorrectQuestion(argv[i]);
		}
		else if (std::string(azColName[i]) == "ANSWER2") {
			questionDB.setanswer2(argv[i]);
		}
		else if (std::string(azColName[i]) == "ANSWER3") {
			questionDB.setanswer3(argv[i]);
		}
		else if (std::string(azColName[i]) == "ANSWER4") {
			questionDB.setanswer4(argv[i]);
		}
	}
	if (questionDB.getQuestions() != "none" && questionDB.getCorrectQuestion() != "none" && questionDB.getanswer2() != "none" && questionDB.getanswer3() != "none" && questionDB.getanswer4() != "none")
	{
		questionList.push_back(questionDB);
	}

	return 0;
}

//the function will be in use from getting info from DB from statistics table, will put the data in a list of statistics
//input:?
//output: 0
int SqliteDataBase::callback_statistics(void* data, int argc, char** argv, char** azColName)
{
	Statistics statisticsDB("none", -1, -1, -1, -1);
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "USER_NAME") {
			statisticsDB.setUserName(argv[i]);
		}
		else if (std::string(azColName[i]) == "PLAY_TIME") {
			statisticsDB.setPalyTime(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NUM_CORRECT") {
			statisticsDB.setCorrect(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NUM_TOTAL") {
			statisticsDB.setTotal(atoi(argv[i]));
		}
		else if (std::string(azColName[i]) == "NUM_GAMES") {
			statisticsDB.setGames(atoi(argv[i]));
		}
	}
	if (statisticsDB.getUserName() != "none" && statisticsDB.getPalyTime() != -1 && statisticsDB.getCorrect() != -1 && statisticsDB.getTotal() != -1 && statisticsDB.getGames() != -1)
	{
		statisticsList.push_back(statisticsDB);
	}
	return 0;
}

//will put in the statistics list the user statistics
//input: userName
//output: none
void SqliteDataBase::getStatistics(std::string userName)
{
	std::string code = GET_USER_STATISTICS;
	std::string replace_code = USER_NAME;
	code.replace(code.find(replace_code), replace_code.length(), userName);

	const char* sqlStatement = code.c_str();
	char* errMessage = nullptr;
	std::lock_guard<std::mutex> locker(dbAccess);         //mutex
	statisticsList.clear();
	int res = sqlite3_exec(db, sqlStatement, this->callback_statistics, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		throw std::invalid_argument("Failed to get statistics\n");
	}
	if (statisticsList.empty())
	{
		throw std::string("Failed to find user statistics\n");
	}
}

//will create a table in the DB
//input: the table as a string
//output: true -> created, false -> create faild
bool SqliteDataBase::create_table(std::string table)
{
	const char* sqlStatement = table.c_str();
	char* errMessage = nullptr;
	int res = sqlite3_exec(db, sqlStatement, nullptr, nullptr, &errMessage);//the table command
	if (res != SQLITE_OK)//if the table opened bad
	{
		sqlite3_close(db);
		db = nullptr;
		throw std::invalid_argument("Failed to make table\n");
	}
	return true;

}
