#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
#include "User.h"
#include "Statistics.h"
#include <io.h>
#include <mutex>

#define DB_FILE_NAME "TriviaDB.sqlite"

/*users*/
#define USERS_CREATE "CREATE TABLE IF NOT EXISTS USERS(\
					   NAME TEXT PRIMARY KEY NOT NULL,\
					   PASSWORD TEXT NOT NULL,\
					   MAIL INTEGER NOT NULL);"

#define USER_NAME "<user_name>"
#define USER_PASSWORD "<user_password>"
#define USER_MAIL "<user_mail>"

#define ADD_USER "insert into USERS(name, password, mail) values ('<user_name>', '<user_password>', '<user_mail>');"
#define DOES_USER_EXISTS_BY_USER_NAME "select * from users where users.name like '<user_name>';"
#define DOES_USER_EXISTS_BY_USER_NAME_AND_PASSWORD "select * from users where users.name like '<user_name>' and users.password like '<user_password>';"


/*questions*/
//answer1 is the correct answer
#define QUESTIONS_CREATE "CREATE TABLE IF NOT EXISTS QUESTIONS(\
                          QUESTION TEXT PRIMARY KEY NOT NULL,\
                          ANSWER1 TEXT NOT NULL,\
                          ANSWER2 TEXT NOT NULL,\
                          ANSWER3 TEXT NOT NULL,\
                          ANSWER4 TEXT NOT NULL);"

#define GET_QEUSTIONS "select * from questions;"


/*statistics*/
#define STATISTICS_CREATE "CREATE TABLE IF NOT EXISTS STATISTICS(\
						  USER_NAME TEXT PRIMARY KEY NOT NULL,\
						  PLAY_TIME INTEGER NOT NULL,\
						  NUM_CORRECT INTEGER NOT NULL,\
						  NUM_TOTAL INTEGER NOT NULL,\
						  NUM_GAMES INTEGER NOT NULL,\
						  FOREIGN KEY(USER_NAME) REFERENCES USERS(NAME));"

#define GET_USER_STATISTICS "select * from statistics where statistics.user_name like '<user_name>';"


class SqliteDataBase : public IDatabase
{
public:
	SqliteDataBase();
	~SqliteDataBase();
	/*users*/
	void addNewUser(std::string username, std::string password, std::string mail) override;
	bool doesUserExists(std::string username) override;
	bool doesPasswordMatch(std::string password, std::string userName) override;
	/*questions*/
	std::list<Questions> getQuestions(int numOfquestion) override;
	/*statistics*/
	float getPlayerAverageAnswerTime(std::string userName) override;
	int getNumOfCorrectAnswers(std::string userName) override;
	int getNumOfTotalAnswers(std::string userName) override;
	int getNumOfPlayerGames(std::string userName) override;
	std::list<Statistics> getAllStatistics() override;//////


private:
	sqlite3* db;

	static int callback_users(void* data, int argc, char** argv, char** azColName);
	static int callback_questions(void* data, int argc, char** argv, char** azColName);
	static int callback_statistics(void* data, int argc, char** argv, char** azColName);

	void getStatistics(std::string userName);

	bool create_table(std::string table);
	bool open();
};