#include "Statistics.h"

Statistics::Statistics(std::string userName, int palyTime, int correct, int total, int games)
{
	_userName = userName;
	_palyTime = palyTime;
	_correct = correct;
	_total = total;
	_games = games;
}

const std::string& Statistics::getUserName()
{
	return _userName;
}

void Statistics::setUserName(const std::string& userName)
{
	_userName = userName;
}

int Statistics::getPalyTime()
{
	return _palyTime;
}

void Statistics::setPalyTime(int palyTime)
{
	_palyTime = palyTime;
}

int Statistics::getCorrect()
{
	return _correct;
}

void Statistics::setCorrect(int correct)
{
	_correct = correct;
}

int Statistics::getTotal()
{
	return _total;
}

void Statistics::setTotal(int total)
{
	_total = total;
}

int Statistics::getGames()
{
	return _games;
}

void Statistics::setGames(int games)
{
	_games = games;
}
