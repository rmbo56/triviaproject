#pragma once
#include <string>
#include <iostream>
#include <list>

class Statistics;
typedef std::list<Statistics>::iterator statistics_iter;

class Statistics
{
public:
	Statistics(std::string userName, int palyTime, int correct, int total, int games);

	const std::string& getUserName();
	void setUserName(const std::string& userName);

	int getPalyTime();
	void setPalyTime(int palyTime);

	int getCorrect();
	void setCorrect(int correct);

	int getTotal();
	void setTotal(int total);

	int getGames();
	void setGames(int games);

private:
	std::string _userName;
	int _palyTime;//in seconds
	int _correct;
	int _total;
	int _games;
};