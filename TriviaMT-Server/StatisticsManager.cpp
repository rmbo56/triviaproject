#include "StatisticsManager.h"

std::mutex statisticsDataAccess;

StatisticsManager::StatisticsManager(IDatabase* database) :
	_database(database)
{
}

std::vector<std::string> StatisticsManager::getHighScore()
{
	std::vector<std::string> userScores;

	std::unique_lock<std::mutex> locker(statisticsDataAccess);        //mutex
	std::list<Statistics> allStatistics = _database->getAllStatistics();
	locker.unlock();

	//the amount of Statistics in the DB
	int amount = 0;
	for (auto const& i : allStatistics)
	{
		amount++;
	}
	if (amount < 4)
	{
		throw std::string("can not find 5 users\n");
	}
	//will put the Statistics in an array

	int* saveAll = new int[amount];
	int j = 0;
	for (auto const& i : allStatistics)
	{
		saveAll[j] = calculateScore(i);
		j++;
	}

	//will give the X biggest scores from all the scores
	int i;
	int score[AMOUNT_OF_SCORES];
	int max = 0, index;

	for (int j = 0; j < AMOUNT_OF_SCORES; j++) 
	{
		max = saveAll[0];
		index = 0;
		for (i = 1; i < amount; i++)
		{
			if (max < saveAll[i])
			{
				max = saveAll[i];
				index = i;
			}
		}
		score[j] = max;
		saveAll[index] = INT_MIN;
	}

	
	//will put in a list the 5 biggest scores 
	std::string text = "";
	//1
	text = FIRST_PLACE;
	text += ":";
	text += GetUsernameByScore(allStatistics, score[0]);
	text += "-";
	text += std::to_string(score[0]);
	userScores.push_back(text);
	//2
	text = SECOND_PLACE;
	text += ":";
	text += GetUsernameByScore(allStatistics, score[1]);
	text += "-";
	text += std::to_string(score[1]);
	userScores.push_back(text);
	//3
	text = THIRD_PLACE;
	text += ":";
	text += GetUsernameByScore(allStatistics, score[2]);
	text += "-";
	text += std::to_string(score[2]);
	userScores.push_back(text);
	//4
	text = FOURTH_PLACE;
	text += ":";
	text += GetUsernameByScore(allStatistics, score[3]);
	text += "-";
	text += std::to_string(score[3]);
	userScores.push_back(text);
	//5
	text = FIFTH_PLACE;
	text += ":";
	text += GetUsernameByScore(allStatistics, score[4]);
	text += "-";
	text += std::to_string(score[4]);
	userScores.push_back(text);


	delete[] saveAll;
	return userScores;
}

std::string StatisticsManager::GetUsernameByScore(std::list<Statistics> allStatistics, int score)
{
	for (auto const& i : allStatistics)
	{
		if (calculateScore(i) == score)
		{
			Statistics statistic = i;
			return statistic.getUserName();
		}
	}
}

//will give a user statistics by hus user name
//input:username
//output: a vector of string(name:data) in this order: AVGtime->correct->total->gamesPlayed
std::vector<std::string> StatisticsManager::getUserStatistics(std::string username)
{
	std::vector<std::string> userStatistics;
	//AVGtime
	std::string toStatistics = AVG_ANS_TIME;
	toStatistics += ":";
	toStatistics += std::to_string(_database->getPlayerAverageAnswerTime(username));
	userStatistics.push_back(toStatistics);

	//correct
	toStatistics = CORRECT_ANS;
	toStatistics += ":";
	toStatistics += std::to_string(_database->getNumOfCorrectAnswers(username));
	userStatistics.push_back(toStatistics);

	//total
	toStatistics = TOTAL_ANS;
	toStatistics += ":";
	toStatistics += std::to_string(_database->getNumOfTotalAnswers(username));
	userStatistics.push_back(toStatistics);

	//gamesPlayed
	toStatistics = AMOUNT_OF_GAMES;
	toStatistics += ":";
	toStatistics += std::to_string(_database->getNumOfPlayerGames(username));
	userStatistics.push_back(toStatistics);


	return userStatistics;
}

int StatisticsManager::calculateScore(Statistics statistic)
{
	int score = statistic.getCorrect() * 10;//correct answer gives 10 points
	score += (statistic.getTotal() - statistic.getCorrect()) * -2; //incorrect answer lowers 2 points
	return score;
}