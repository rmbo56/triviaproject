#pragma once
#include "IDatabase.h"
#include "ProtocolDATA.h"
#include <vector>
#include <mutex>

#define AVG_ANS_TIME "avgTime"
#define CORRECT_ANS "correct"
#define TOTAL_ANS "total"
#define AMOUNT_OF_GAMES "games"

#define FIRST_PLACE "first"
#define SECOND_PLACE "second"
#define THIRD_PLACE "third"
#define FOURTH_PLACE "fourth"
#define FIFTH_PLACE "fifth"

#define AMOUNT_OF_SCORES 5

class IDatabase;
class Statistics;

class StatisticsManager
{
public:
	StatisticsManager(IDatabase* database);

	std::vector<std::string> getHighScore();
	std::vector<std::string> getUserStatistics(std::string username);

private:
	IDatabase* _database;

	int calculateScore(Statistics statistic);
	std::string GetUsernameByScore(std::list<Statistics> allStatistics, int score);
	

};