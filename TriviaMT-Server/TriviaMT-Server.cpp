﻿#pragma comment(lib, "Ws2_32.lib")
#include "WSAInitializer.h"
#include "Server.h"


int main()
{
    try
    {
        WSAInitializer wasInit;
        Server triviaServer;

        triviaServer.run();
    }
    catch (std::exception e)
    {
        std::cout << "Error occured: " << e.what() << std::endl;
    }


    system("pause");

    return 0;
}