#include "User.h"
#include <iomanip>


User::User(const std::string& mail, const std::string& password, const std::string& name) :
	m_mail(mail), m_password(password), m_name(name)
{
	// Left empty
}

const std::string& User::getPassword() const
{
	return m_password;
}

void User::setPassword(const std::string& password)
{
	m_password = password;
}



const std::string& User::getName() const
{
	return m_name;
}

void User::setName(const std::string& name)
{
	m_name = name;
}

const std::string& User::getMail() const
{
	return m_mail;
}

void User::setMail(const std::string& mail)
{
	m_mail = mail;
}

bool User::operator==(const User& other) const
{
	return m_name == other.getName();
}

bool User::operator==(std::string name) const {
	return m_name == name;
}

bool User::operator<(const User& oUser) const
{
	return m_name < oUser.getName();
}

std::ostream& operator<<(std::ostream& strOut, const User& user) {
	strOut <<  "@" << user.getName() << " - " << user.getMail() << " - " << user.getPassword();
	return strOut;
}

