#pragma once
#include <string>
#include <iostream>
#include <list>

class User;
typedef std::list<User>::iterator user_iter;

class User
{
public:

	User(const std::string& mail, const std::string& password, const std::string& name);

	const std::string& getPassword() const;
	void setPassword(const std::string& password);

	const std::string& getName() const;
	void setName(const std::string& name);

	const std::string& getMail() const;
	void setMail(const std::string& mail);

	bool operator==(const User& other) const;
	bool operator==(std::string name) const;
	bool operator<(const User& other) const;
	friend std::ostream& operator<<(std::ostream& str, const User& other);

private:
	std::string m_name;
	std::string m_password;
	std::string m_mail;
};

